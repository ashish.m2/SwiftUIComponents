//
//  ConnectPitchView.swift
//  ComponentCreation (iOS)
//
//  Created by Ashish Mankar on 01/03/22.
//

import SwiftUI

struct ConnectPitchView: View {
    let viewModel:MatchesCardViewModel = MatchesCardViewModel()
    var body: some View {
        ZStack{
            VStack{
                PremiumPitchBackgroundCurve()
                    .frame(width: self.viewModel.getCardWidth())
                Spacer()
            }
            VStack(spacing: 0){
                HStack{
                    Spacer()
                    Button(action: {}, label: {
                        Image("closeOnBoarding")
                            .renderingMode(.template)
                            .foregroundColor(Color(UIColor.Theme.primaryContrast))
                            .frame(width: 20, height: 20)
                            .padding(.horizontal, 16)
                            .padding(.vertical, UIScreen.main.bounds.width/20)
                    })
                }
                Text("You’ve sent a Connect to Amit!")
                    .font(.system(size: 26, weight: .bold, design: .default))
                    .foregroundColor(Color(UIColor.Theme.primaryContrast))
                    .multilineTextAlignment(.center)
                    .padding(.horizontal, 48)
                    .padding(.bottom, 28)
                    .lineLimit(2)
                    .minimumScaleFactor(0.01)
                Image("demomale")
                    .resizable()
                    .frame(width: self.viewModel.getConnectPremiumPitchPhotoHieghtWidth(), height: self.viewModel.getConnectPremiumPitchPhotoHieghtWidth())
                    .cornerRadius(self.viewModel.getRadiusForPremiumPitchPhoto())
                Text("Why wait? Contact him directly on")
                    .font(.system(size: 16, weight: .bold, design: .default))
                    .foregroundColor(Color(UIColor.Neutrals.primary))
                    .lineLimit(1)
                    .minimumScaleFactor(0.01)
                    .padding(.top, 24)
                Text("+91 9812XXXXXX")
                    .font(.system(size: 12, weight: .regular, design: .default))
                    .foregroundColor(Color(UIColor.Neutrals.tertiary))
                    .padding(.top, 8)
                Spacer()
                Button(action: {}, label: {
                    ZStack{
                        Text("Upgrade to Premium")
                            .font(.system(size: 16, weight: .bold, design: .default))
                            .foregroundColor(Color(UIColor.Theme.primaryContrast))
                            .padding(.horizontal, 16)
                            .padding(.vertical, 12)
                    }
                    .background(Color(UIColor.Theme.primary))
                    .cornerRadius(22)
                    .shadow(color: Color(UIColor.Theme.primary).opacity(0.5), radius: 5, x: 0, y: 4)
                })
                    .padding(.bottom, 40)
            }
        }
        .background(Color(UIColor.Theme.primaryContrast))
        .frame(width: self.viewModel.getCardWidth(), height: self.viewModel.getCardHeight())
        .cornerRadius(8)
        .shadow(color: .black.opacity(0.5), radius: 10, x: 5, y: 5)
        
    }
}

struct PremiumPitchBackgroundCurve: View {
    let viewModel:MatchesCardViewModel = MatchesCardViewModel()
    var body: some View {
        ZStack {
            LinearGradient(colors: [Color(UIColor.RedGradient.top),Color(UIColor.RedGradient.bottom)], startPoint: .top, endPoint: .bottom)
                .clipShape(path(in: CGRect(x: 0, y: 0, width: self.viewModel.getCardWidth(), height: self.viewModel.getPitchBackgroundHeight()), margin: self.viewModel.getPitchMargin())
                            .offset(x: -viewModel.getPitchMargin()))
        }
        .frame(width: self.viewModel.getCardWidth(),height: self.viewModel.getPitchBackgroundHeight() + 36)
        .cornerRadius(10)
    }
    
    func path(in rect: CGRect, margin: CGFloat) -> Path {
        var path = Path()
        let rectMinX = rect.minX + margin
        let rectMaxX = rect.maxX + margin
        path.move(to: CGPoint(x: rectMinX, y: rect.minY))
        path.addLine(to: CGPoint(x: rectMaxX, y: rect.minY))
        path.addLine(to: CGPoint(x: rectMaxX, y: rect.maxY))
        path.addQuadCurve(to: CGPoint(x: rectMinX, y: rect.maxY), control: CGPoint(x: rect.midX + margin, y: rect.maxY + 72))
        return path
    }
}

struct ConnectPitchView_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            PremiumPitchBackgroundCurve()
            ConnectPitchView()
        }
    }
}
