//
//  SwipeOnBoarding.swift
//  ComponentCreation (iOS)
//
//  Created by Ashish Mankar on 11/02/22.
//

import SwiftUI

struct SwipeOnBoardingViewModel {
    
}

struct SwipeOnBoarding: View {
    var viewModel: SwipeOnBoardingViewModel = SwipeOnBoardingViewModel()
    var body: some View {
        Text("Hello, World!")
    }
}

struct SwipeOnBoarding_Previews: PreviewProvider {
    static var previews: some View {
        SwipeOnBoarding()
    }
}
