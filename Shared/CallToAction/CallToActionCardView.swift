//
//  CallToActionCardView.swift
//  ComponentCreation (iOS)
//
//  Created by Ashish Mankar on 14/02/22.
//

import SwiftUI

struct CallToActionCardView: View {
    var body: some View {
        ZStack {
            Color(.black).opacity(0.2)
            VStack{
                ActionUnhideView(rightCTAAction: {})
                ActionEOIView(leftCTAAction: {}, rightCTAAction: {})
                ActionEOIFreeAccessView(leftCTAAction: {}, rightCTAAction: {})
                ActionEOIJustJoinView(leftCTAAction: {}, rightCTAAction: {})
                ActionEOIVIPView(leftCTAAction: {}, rightCTAAction: {})
                DeletedProfileView()
            }
            .background(Color(.black).opacity(0.5))
        }
    }
}

struct ActionEOIView: View {
    var viewModel:MatchesCardViewModel = MatchesCardViewModel()
    var leftCTAAction:() -> ()
    var rightCTAAction:() -> ()
    var body: some View {
        let width:CGFloat = viewModel.getCardWidth()
        let ctaWidth:CGFloat = viewModel.getCTARadius()
        let ctaMargin:CGFloat = viewModel.getCTAMargin()
        ZStack(alignment: .top){
            Image("cta_curve")
                .resizable()
                .scaledToFit()
                .foregroundColor(.white)
                .offset(y: 16)
            HStack(spacing: 0){
                Color(.clear)
                    .frame(width: ctaMargin)
                Button(action: leftCTAAction, label: {
                    VStack{
                        Image("declineCancel")
                            .resizable()
                            .scaledToFit()
                            .frame(width: ctaWidth, height: ctaWidth)
                        Text("Not Now")
                            .foregroundColor(Color(viewModel.ctaNotNowColor()))
                            .font(.system(size: 11, weight: .regular, design: .default))
                    }
                })
                Spacer()
                Button(action: rightCTAAction, label: {
                    VStack{
                        Image("callToAction_Connect")
                            .resizable()
                            .scaledToFit()
                            .frame(width: ctaWidth, height: ctaWidth)
                        Text("Connect")
                            .foregroundColor(Color(viewModel.ctaConnectColor()))
                            .font(.system(size: 11, weight: .medium, design: .default))
                    }
                })
                Color(.clear)
                    .frame(width: ctaMargin)
            }
        }
        .frame(width: width,height: 104)
    }
}

struct ActionUnhideView: View {
    var viewModel:MatchesCardViewModel = MatchesCardViewModel()
    var rightCTAAction:() -> ()
    @State var tooltipFlag:Bool = true
    var body: some View {
        let width:CGFloat = viewModel.getCardWidth()
        let ctaMargin:CGFloat = viewModel.getCTAMargin()
        ZStack(alignment: .top){
            if tooltipFlag {
                ToolTipUnhideCTA()
                    .onTapGesture {
                        withAnimation {
                            tooltipFlag.toggle()
                        }
                    }
            }
            HStack(spacing: 8){
                Color(.clear)
                    .frame(width: ctaMargin)
                Text("Your Profile is hidden")
                    .foregroundColor(Color(UIColor.Neutrals.tertiary))
                    .font(.system(size: 14, weight: .regular, design: .default))
                    .italic()
                    .lineLimit(1)
                    .minimumScaleFactor(0.5)
                Button(action: {}, label: {
                    ZStack{
                        Color(UIColor.Accent.primary)
                        Text("Unhide Now")
                            .foregroundColor(Color(UIColor.Theme.primaryContrast))
                            .padding(EdgeInsets(top: 8, leading: 10, bottom: 8, trailing: 10))
                            .font(.system(size: 16, weight: .bold, design: .default))
                            .minimumScaleFactor(0.5)
                    }
                    .frame(height: 40)
                })
                    .cornerRadius(20)
                Color(.clear)
                    .frame(width: ctaMargin)
            }
        }
        .frame(width: width,height: 92)
    }
}

struct ActionEOIFreeAccessView: View {
    var viewModel:MatchesCardViewModel = MatchesCardViewModel()
    var leftCTAAction:() -> ()
    var rightCTAAction:() -> ()
    @State var toolTipFlag:Bool = false
    var body: some View {
        let width:CGFloat = viewModel.getCardWidth()
        let ctaWidth:CGFloat = viewModel.getCTARadius()
        let ctaMargin:CGFloat = viewModel.getCTAMargin()
        let freeAccessWidth: CGFloat = width/3.2
        ZStack(alignment: .top){
            if toolTipFlag {
                ToolTipCTA()
                    .onTapGesture {
                        withAnimation {
                            toolTipFlag.toggle()
                        }
                    }
            }
            Image("cta_freeaccess_curve")
                .resizable()
                .scaledToFit()
                .foregroundColor(.white)
                .offset(y: 16)
            HStack(spacing: 0){
                Color(.clear)
                    .frame(width: ctaMargin)
                Button(action: leftCTAAction, label: {
                    VStack{
                        Image("declineCancel")
                            .resizable()
                            .scaledToFit()
                            .frame(width: ctaWidth, height: ctaWidth)
                        Text("Not Now")
                            .foregroundColor(Color(viewModel.ctaNotNowColor()))
                            .font(.system(size: 11, weight: .regular, design: .default))
                    }
                })
                Spacer()
                ZStack{
                    Button(action: {
                        withAnimation {
                            toolTipFlag.toggle()
                        }
                    }, label: {
                        let freeAccessIcon = toolTipFlag ? "tag_free_access_highlight" : "tag_free_access"
                        Image(freeAccessIcon)
                            .resizable()
                            .scaledToFit()
                    })
                        .frame(width: freeAccessWidth)
                        .offset(y: 2)
                    
                }
                .frame(width: viewModel.getVIPTagWidth(),height: 32)
                .offset(y: -7.5)
                Spacer()
                Button(action: rightCTAAction, label: {
                    VStack{
                        Image("callToAction_Connect")
                            .resizable()
                            .scaledToFit()
                            .frame(width: ctaWidth, height: ctaWidth)
                        Text("Connect")
                            .foregroundColor(Color(viewModel.ctaConnectColor()))
                            .font(.system(size: 11, weight: .medium, design: .default))
                    }
                })
                Color(.clear)
                    .frame(width: ctaMargin)
            }
        }
        .frame(width: width,height: 92)
    }
}

struct ActionEOIJustJoinView: View {
    var viewModel:MatchesCardViewModel = MatchesCardViewModel()
    var leftCTAAction:() -> ()
    var rightCTAAction:() -> ()
    var body: some View {
        let width:CGFloat = viewModel.getCardWidth()
        let ctaWidth:CGFloat = viewModel.getCTARadius()
        let ctaMargin:CGFloat = viewModel.getCTAMargin()
        ZStack(alignment: .top){
            Image("cta_curve")
                .resizable()
                .scaledToFit()
                .foregroundColor(.white)
                .offset(y: 16)
            HStack(spacing: 0){
                Color(.clear)
                    .frame(width: ctaMargin)
                Button(action: leftCTAAction, label: {
                    VStack{
                        Image("declineCancel")
                            .resizable()
                            .scaledToFit()
                            .frame(width: ctaWidth, height: ctaWidth)
                        Text("Not Now")
                            .foregroundColor(Color(viewModel.ctaNotNowColor()))
                            .font(.system(size: 11, weight: .regular, design: .default))
                    }
                })
                Spacer()
                Button(action: rightCTAAction, label: {
                    VStack{
                        ZStack{
                            Color(UIColor.Theme.primaryContrast)
                                .cornerRadius(ctaWidth/2)
                            Image(uiImage: viewModel.getJustJoinTickIcon())
                                .resizable()
                                .scaledToFit()
                                .frame(width: 24, height: 20)
                                .zIndex(1)
                        }
                        .frame(width: ctaWidth, height: ctaWidth)
                        Text("Connect")
                            .foregroundColor(Color(viewModel.ctaJustJoinConnectColor()))
                            .font(.system(size: 11, weight: .medium, design: .default))
                    }
                })
                Color(.clear)
                    .frame(width: ctaMargin)
            }
        }
        .frame(width: width,height: 92)
    }
}

struct ActionEOIVIPView: View {
    var viewModel:MatchesCardViewModel = MatchesCardViewModel()
    var leftCTAAction:() -> ()
    var rightCTAAction:() -> ()
    var body: some View {
        let width:CGFloat = viewModel.getCardWidth()
        let ctaWidth:CGFloat = viewModel.getCTARadius()
        let ctaMargin:CGFloat = viewModel.getCTAMargin()
        ZStack(alignment: .top){
            Image("cta_vip_curve")
                .resizable()
                .scaledToFit()
                .foregroundColor(.white)
                .offset(y: 16)
            HStack(spacing: 0){
                Color(.clear)
                    .frame(width: ctaMargin)
                Button(action: leftCTAAction, label: {
                    VStack{
                        Image("declineCancel")
                            .resizable()
                            .scaledToFit()
                            .frame(width: ctaWidth, height: ctaWidth)
                        Text("Not Now")
                            .foregroundColor(Color(viewModel.ctaNotNowColor()))
                            .font(.system(size: 11, weight: .regular, design: .default))
                    }
                })
                Spacer()
                ZStack{
                    Image("vip_consultant_tag")
                        .resizable()
                    Text("Profile Managed by VIP Consultant")
                        .foregroundColor(Color(UIColor.Other.gold))
                        .font(.system(size: 10, weight: .regular, design: .default))
                        .italic()
                        .lineLimit(2)
                        .multilineTextAlignment(.center)
                }
                .frame(width: viewModel.getVIPTagWidth(),height: 32)
                .offset(y: -7.5)
                Spacer()
                Button(action: rightCTAAction, label: {
                    VStack{
                        Image(uiImage: viewModel.getVIPIcon())
                            .resizable()
                            .scaledToFit()
                            .frame(width: ctaWidth, height: ctaWidth)
                        Text("Connect")
                            .foregroundColor(Color(viewModel.getVIPCaptionColor()))
                            .font(.system(size: 11, weight: .medium, design: .default))
                    }
                })
                Color(.clear)
                    .frame(width: ctaMargin)
            }
        }
        .frame(width: width,height: 92)
    }
}

struct CallToActionCardView_Previews: PreviewProvider {
    static var previews: some View {
        CallToActionCardView()
    }
}

struct LeftStroke: View {
    var viewModel:MatchesCardViewModel = MatchesCardViewModel()
    var body: some View {
        Path{ path in
            path.move(to: CGPoint(x: 0, y: 0))
            path.addCurve(to: CGPoint(x: 37, y: 22), control1: CGPoint(x: 0, y: 0), control2: CGPoint(x: 0, y: 12))
        }
        .stroke()
        .foregroundColor(Color(viewModel.ctaCurveColor()).opacity(0.5))
        .offset(y: 10)
    }
}

struct MiddleStroke: View {
    var viewModel:MatchesCardViewModel = MatchesCardViewModel()
    var body: some View {
        let width:CGFloat = viewModel.getCardWidth()
        Path{ path in
            path.move(to: CGPoint(x: width, y: 0))
            path.addCurve(to: CGPoint(x: width - 37, y: 22), control1: CGPoint(x: width, y: 0), control2: CGPoint(x: width, y: 12))
        }
        .stroke()
        .foregroundColor(Color(viewModel.ctaCurveColor()).opacity(0.5))
        .offset(y: 10)
    }
}

struct RightStroke: View {
    var viewModel:MatchesCardViewModel = MatchesCardViewModel()
    var body: some View {
        let width:CGFloat = viewModel.getCardWidth()
        Path{ path in
            path.move(to: CGPoint(x: 92, y: 28))
            path.addCurve(to: CGPoint(x: width - 92, y: 28), control1: CGPoint(x: 108, y: 32), control2: CGPoint(x: width - 108, y: 32))
        }
        .stroke()
        .foregroundColor(Color(viewModel.ctaCurveColor()).opacity(0.5))
        .offset(y: 10)
    }
}

struct ToolTipCTA: View {
    var viewModel:MatchesCardViewModel = MatchesCardViewModel()
    var body: some View {
        let toolTipWidth = viewModel.getCTAToolTipWidth()
        ZStack(alignment: .top){
            ZStack{
                Color(UIColor.Theme.primaryContrast)
                Text("You can Contact him for free after sending a Connect.")
                    .foregroundColor(Color(UIColor.Other.Gray.light))
                    .font(.system(size: 14, weight: .regular, design: .default))
                    .minimumScaleFactor(0.5)
                    .lineLimit(2)
                    .lineSpacing(6)
                    .padding(EdgeInsets(top: 8, leading: 16, bottom: 10, trailing: 16))
            }
            .frame(width: toolTipWidth, height: 68)
            .cornerRadius(4)
            .offset(y: -68)
            Image("coachUpArrow")
                .renderingMode(.template)
                .foregroundColor(Color(UIColor.Theme.primaryContrast))
                .rotationEffect(.degrees(180))
        }
    }
}

struct ToolTipUnhideCTA: View {
    var viewModel:MatchesCardViewModel = MatchesCardViewModel()
    var body: some View {
        let toolTipWidth = viewModel.getCTAToolTipWidth()
        ZStack(alignment: .topTrailing){
            ZStack{
                Color(UIColor.Theme.primaryContrast)
                Group{
                    Text("You will need to ")
                    +
                    Text("unhide")
                        .bold()
                    +
                    Text(" your Profile to swipe on people.")
                }
                    .foregroundColor(Color(UIColor.Other.Gray.light))
                    .font(.system(size: 14, weight: .regular, design: .default))
                    .minimumScaleFactor(0.5)
                    .multilineTextAlignment(.center)
                    .lineLimit(2)
                    .lineSpacing(6)
                    .padding(EdgeInsets(top: 8, leading: 16, bottom: 10, trailing: 16))
            }
            .frame(width: toolTipWidth, height: 68)
            .cornerRadius(4)
            .offset(y: -68)
            Image("coachUpArrow")
                .renderingMode(.template)
                .foregroundColor(Color(UIColor.Theme.primaryContrast))
                .rotationEffect(.degrees(180))
                .padding(EdgeInsets(top: 0, leading: 0, bottom: 0, trailing: 42))
        }
    }
}

struct DeletedProfileView: View {
    var viewModel:MatchesCardViewModel = MatchesCardViewModel()
    var body: some View {
        let width:CGFloat = viewModel.getCardWidth()
        let ctaMargin:CGFloat = viewModel.getCTAMargin()
        ZStack(alignment: .top){
            HStack(spacing: 8){
                Color(.clear)
                    .frame(width: ctaMargin)
                ZStack{
                    Text("Prospect has deleted his Profile")
                        .foregroundColor(Color(UIColor.Theme.primaryContrast))
                        .font(.system(size: 14, weight: .medium, design: .default))
                        .lineLimit(1)
                        .minimumScaleFactor(0.5)
                        .padding(.vertical, 10)
                        .padding(.horizontal, 20)
                }
                .background(Color(UIColor.Other.blackShade).opacity(48))
                .cornerRadius(18)
                Color(.clear)
                    .frame(width: ctaMargin)
            }
        }
        .frame(width: width,height: 92)
    }
}
