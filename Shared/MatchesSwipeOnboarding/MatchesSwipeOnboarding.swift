//
//  MatchesSwipeOnboarding.swift
//  ComponentCreation (iOS)
//
//  Created by Ashish Mankar on 16/02/22.
//

import SwiftUI

struct MatchesSwipeOnboarding: View {
    var viewModel:MatchesCardViewModel = MatchesCardViewModel()
    @State private var showSwipeLeftRight = true
    @State private var tapToSend = false
    @State private var tapToDecide = false
    @State private var tapToSeeProfile = false
    var body: some View {
        ZStack{
            if tapToSeeProfile {
                TapToSeeTheirProfile(flag: $tapToSeeProfile)
                    .zIndex(0.7)
            }
            if showSwipeLeftRight {
                SwipeLeftOrRight(flag: $showSwipeLeftRight,nextCardFlag: $tapToSeeProfile)
                    .zIndex(1)
            }
        }
    }
}

struct MatchesSwipeOnboarding_Previews: PreviewProvider {
    static var previews: some View {
        MatchesSwipeOnboarding()
    }
}

struct SwipeLeftOrRight: View {
    var viewModel:MatchesCardViewModel = MatchesCardViewModel()
    @Binding var flag: Bool
    @Binding var nextCardFlag:Bool
    @State private var offset = CGSize.zero
    @State private var swipeAngle:CGFloat = -80
    @State private var scale:CGFloat = 0.5
    @State private var swipeImageName:String = "swipe_onboarding"
    @State private var swipeOpacity:CGFloat = 0.2
    @State private var cardOpacity:CGFloat = 0
    @State private var isConnect:Bool = true
    @State private var title = "Swipe Right to send a Connect"
    var body: some View {
        ZStack{
            ZStack{
                Image("male_member")
                    .resizable()
                    .blur(radius: 20)
            }
            ZStack{
                LinearGradient(colors: [Color(UIColor.GreenGradient.start),Color(UIColor.GreenGradient.end)], startPoint: .top, endPoint: .bottom)
                    .opacity(self.isConnect ? 0.3 : 0)
                Color(UIColor.Neutrals.primary)
                    .opacity(self.isConnect ? 0 : 0.3)
            }
            VStack(spacing: 0) {
                ZStack{
                    Text("Swipe Right to send a Connect")
                        .foregroundColor(Color(UIColor.Theme.primaryContrast))
                        .font(.system(size: 28, weight: .bold, design: .default))
                        .italic()
                        .multilineTextAlignment(.center)
                        .padding(EdgeInsets(top: 0, leading: 30, bottom: 40, trailing: 30))
                        .opacity(self.isConnect ? 1 : 0)
                    Text("Swipe Left to decide later")
                        .foregroundColor(Color(UIColor.Theme.primaryContrast))
                        .font(.system(size: 28, weight: .bold, design: .default))
                        .italic()
                        .multilineTextAlignment(.center)
                        .padding(EdgeInsets(top: 0, leading: 30, bottom: 40, trailing: 30))
                        .animation(Animation.easeIn(duration: 0.2))
                        .opacity(self.isConnect ? 0 : 1)
                }
                Image(swipeImageName)
                    .rotationEffect(.degrees(swipeAngle))
                    .scaleEffect(scale)
                    .opacity(swipeOpacity)
            }
            .opacity(cardOpacity)
            
        }
        .background(Color(.gray))
        .frame(width: viewModel.getCardWidth(), height: viewModel.getCardHeight())
        .cornerRadius(8)
        .rotationEffect(.degrees(Double(offset.width/5)))
        .offset(x: offset.width)
        .onAppear {
            let duration:Double = 1.2
            let fastAnimationDuration:Double = 0.4
            DispatchQueue.main.asyncAfter(deadline: .now() + duration, execute: {
                withAnimation {
                    self.cardOpacity = 1
                    self.swipeAngle = 0
                    self.scale = 1
                    self.swipeOpacity = 1
                    DispatchQueue.main.asyncAfter(deadline: .now() + fastAnimationDuration, execute: {
                        withAnimation {
                            self.swipeOpacity = 0
                            self.isConnect = false
                            DispatchQueue.main.asyncAfter(deadline: .now() + fastAnimationDuration, execute: {
                                withAnimation {
                                    self.swipeImageName = "left_swipe_onboarding"
                                    self.swipeAngle = 80
                                    self.scale = 0.5
                                    DispatchQueue.main.asyncAfter(deadline: .now() + fastAnimationDuration, execute: {
                                        withAnimation {
                                            self.swipeAngle = 0
                                            self.scale = 1
                                            self.swipeOpacity = 1
                                            DispatchQueue.main.asyncAfter(deadline: .now() + duration) {
                                                withAnimation {
                                                    self.flag = false
                                                    self.nextCardFlag = true
                                                }
                                            }
                                        }
                                    })
                                }
                            })
                        }
                    })
                    
                }
            })
        }
    }
}

struct TapToSendConnect: View {
    @Binding var flag:Bool
    @Binding var nextCardFlag:Bool
    var viewModel:MatchesCardViewModel = MatchesCardViewModel()
    @State private var offset = CGSize.zero
    @State private var swipeScale = 0.5
    @State private var offSetX:CGFloat = 0
    @State private var offSetY:CGFloat = 0
    @State private var swipeOpacity:CGFloat = 0.1
    @State private var cardOpacity:CGFloat = 0
    var body: some View {
        ZStack{
            ZStack{
                Image("male_member")
                    .resizable()
                    .blur(radius: 20)
            }
            VStack{
                Spacer()
                ActionEOIView(leftCTAAction: {}, rightCTAAction: {})
                    .opacity(0.5)
            }
            .opacity(cardOpacity)
            Color(.black)
                .opacity(0.3)
            VStack{
                VStack{
                    Spacer()
                    Text("Tap to send a Connect")
                        .foregroundColor(Color(UIColor.Theme.primaryContrast))
                        .font(.system(size: 28, weight: .bold, design: .default))
                        .italic()
                        .minimumScaleFactor(0.5)
                        .lineLimit(1)
                    Spacer()
                    HStack{
                        Spacer()
                        Image("swipe_onboarding_tap")
                            .resizable()
                            .frame(width: 84,height: 84)
                            .rotationEffect(.degrees(148))
                            .scaleEffect(swipeScale)
                            .offset(x: offSetX, y: offSetY)
                            .opacity(swipeOpacity)
                        Color(.clear)
                            .frame(width: self.viewModel.getOnboardingIconMargin(), height: 0)
                    }
                }
                HStack(spacing: 0){
                    Spacer()
                    Button(action: {
                        withAnimation {
                            //offset.width = 360
                            //flag = false
                        }
                        
                    }, label: {
                        VStack{
                            Image("callToAction_Connect")
                                .resizable()
                                .scaledToFit()
                                .frame(width: viewModel.getCTARadius(), height: viewModel.getCTARadius())
                            Text("Connect")
                                .foregroundColor(Color(viewModel.ctaConnectColor()))
                                .font(.system(size: 11, weight: .medium, design: .default))
                        }
                    })
                    Color(.clear)
                        .frame(width: viewModel.getCTAMargin())
                }
                .frame(height: 104)
            }
            .opacity(cardOpacity)
        }
        .background(Color(.gray))
        .frame(width: viewModel.getCardWidth(), height: viewModel.getCardHeight())
        .cornerRadius(8)
        .rotationEffect(.degrees(Double(offset.width/5)))
        .offset(x: offset.width)
        .onAppear {
            let duration = 0.5
            let fastAnimationDuration = 0.2
            DispatchQueue.main.asyncAfter(deadline: .now() + duration) {
                withAnimation {
                    self.cardOpacity = 1
                    self.swipeOpacity = 1
                    self.swipeScale = 1
                    DispatchQueue.main.asyncAfter(deadline: .now() + duration) {
                        withAnimation {
                            self.offSetX = -10
                            self.offSetY = -10
                            DispatchQueue.main.asyncAfter(deadline: .now() + fastAnimationDuration) {
                                withAnimation {
                                    self.offSetX = 2
                                    self.offSetY = 2
                                    DispatchQueue.main.asyncAfter(deadline: .now() + fastAnimationDuration) {
                                        withAnimation(.spring()) {
                                            self.offSetX = 0
                                            self.offSetY = 0
                                            DispatchQueue.main.asyncAfter(deadline: .now() + fastAnimationDuration) {
                                                withAnimation(.spring()) {
                                                    self.flag = false
                                                    self.nextCardFlag = true
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

struct TapToDecideLater: View {
    @Binding var flag:Bool
    @Binding var nextCardFlag:Bool
    var viewModel:MatchesCardViewModel = MatchesCardViewModel()
    @State private var offset = CGSize.zero
    @State private var offSetX:CGFloat = 0
    @State private var offSetY:CGFloat = 0
    @State private var swipeScale:CGFloat = 0.5
    @State private var swipeOpacity:CGFloat = 0.1
    @State private var cardOpacity:CGFloat = 0
    var body: some View {
        ZStack{
            ZStack{
                Image("male_member")
                    .resizable()
                    .blur(radius: 20)
            }
            VStack{
                Spacer()
                ActionEOIView(leftCTAAction: {}, rightCTAAction: {})
                    .opacity(0.5)
            }
            .opacity(cardOpacity)
            Color(.black)
                .opacity(0.3)
            VStack{
                VStack{
                    Spacer()
                    Text("Tap to decide later")
                        .foregroundColor(Color(UIColor.Theme.primaryContrast))
                        .font(.system(size: 28, weight: .bold, design: .default))
                        .italic()
                        .minimumScaleFactor(0.5)
                        .lineLimit(1)
                    Spacer()
                    HStack{
                        Color(.clear)
                            .frame(width: self.viewModel.getOnboardingIconMargin(), height: 0)
                        Image("swipe_onboarding_tap")
                            .resizable()
                            .frame(width: 84,height: 84)
                            .rotationEffect(.degrees(-148))
                            .scaleEffect(swipeScale)
                            .offset(x: offSetX, y: offSetY)
                            .opacity(swipeOpacity)
                        Spacer()
                    }
                }
                HStack(spacing: 0){
                    Color(.clear)
                        .frame(width: viewModel.getCTAMargin())
                    Button(action: {
                        
                    }, label: {
                        VStack{
                            Image("declineCancel")
                                .resizable()
                                .scaledToFit()
                                .frame(width: viewModel.getCTARadius(), height: viewModel.getCTARadius())
                            Text("Not Now")
                                .foregroundColor(Color(viewModel.ctaNotNowColor()))
                                .font(.system(size: 11, weight: .regular, design: .default))
                        }
                    })
                    Spacer()
                }
                .frame(height: 104)
            }
            .opacity(cardOpacity)
        }
        .background(Color(.gray))
        .frame(width: viewModel.getCardWidth(), height: viewModel.getCardHeight())
        .cornerRadius(8)
        .rotationEffect(.degrees(Double(offset.width/5)))
        .offset(x: offset.width)
        .onAppear {
            let duration = 0.5
            let fastAnimationDuration = 0.2
            DispatchQueue.main.asyncAfter(deadline: .now() + duration) {
                withAnimation {
                    self.cardOpacity = 1
                    self.swipeOpacity = 1
                    self.swipeScale = 1
                    DispatchQueue.main.asyncAfter(deadline: .now() + duration) {
                        withAnimation {
                            self.offSetX = 10
                            self.offSetY = -10
                            DispatchQueue.main.asyncAfter(deadline: .now() + fastAnimationDuration) {
                                withAnimation {
                                    self.offSetX = -2
                                    self.offSetY = 2
                                    DispatchQueue.main.asyncAfter(deadline: .now() + fastAnimationDuration) {
                                        withAnimation(.spring()) {
                                            self.offSetX = 0
                                            self.offSetY = 0
                                            DispatchQueue.main.asyncAfter(deadline: .now() + fastAnimationDuration) {
                                                withAnimation(.spring()) {
                                                    self.flag = false
                                                    self.nextCardFlag = true
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

struct TapToSeeTheirProfile: View {
    @Binding var flag:Bool
    var viewModel:MatchesCardViewModel = MatchesCardViewModel()
    @State var scale:CGFloat = 1
    @State var circleDiameter:CGFloat = 20
    @State var circleOpacity:CGFloat = 0
    @State var handOpacity:CGFloat = 0.5
    @State var handScale:CGFloat = 1.5
    @State private var cardOpacity:CGFloat = 0
    var body: some View {
        ZStack{
            ZStack{
                Image("male_member")
                    .resizable()
                    .blur(radius: 20)
            }
            Color(.black)
                .opacity(0.3)
            VStack{
                VStack(spacing: 60){
                    Spacer()
                    Text("Tap to see their Profile")
                        .foregroundColor(Color(UIColor.Theme.primaryContrast))
                        .font(.system(size: 28, weight: .bold, design: .default))
                        .italic()
                        .minimumScaleFactor(0.5)
                        .lineLimit(1)
                    HStack{
                        Image("swipe_onboarding_tap")
                            .resizable()
                            .frame(width: 84,height: 84)
                            .rotationEffect(.degrees(-20))
                            .opacity(handOpacity)
                            .scaleEffect(handScale)
                    }
                    Spacer()
                }
            }
            .opacity(cardOpacity)
            Circle()
                .strokeBorder(Color(.white),lineWidth: 8)
                .frame(width: circleDiameter)
                .offset(x: -24, y: 4)
                .opacity(circleOpacity)
        }
        .background(Color(.gray))
        .frame(width: viewModel.getCardWidth(), height: viewModel.getCardHeight())
        .cornerRadius(8)
        .scaleEffect(scale)
        .onAppear {
            let duration = 0.5
            DispatchQueue.main.asyncAfter(deadline: .now() + duration) {
                withAnimation {
                    self.cardOpacity = 1
                    self.handOpacity = 1
                    self.handScale = 1
                    self.circleOpacity = 0
                    DispatchQueue.main.asyncAfter(deadline: .now() + duration) {
                        withAnimation {
                            DispatchQueue.main.asyncAfter(deadline: .now() + duration) {
                                self.circleOpacity = 0.1
                                withAnimation {
                                    self.circleDiameter = self.viewModel.getCardWidth()
                                    self.circleOpacity = 0
                                    DispatchQueue.main.asyncAfter(deadline: .now() + duration) {
                                        withAnimation {
                                            self.circleOpacity = 0
                                            self.cardOpacity = 0
                                            self.flag = false
                                        }
                                    }
                                }}
                            
                        }
                    }
                }
            }
        }
    }
}
