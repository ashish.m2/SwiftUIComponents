//
//  BottomSheetView.swift
//  ComponentCreation (iOS)
//
//  Created by Ashish Mankar on 18/11/21.
//

import SwiftUI

struct BottomSheetView: View {
    var completion:(() -> Void)
    @State var dragYValue:CGFloat = 0
    var body: some View {
        ZStack{
            //BottomSheetBackgroundView()
            BottomSheetComponentView(completion: {
                completion()
            })
        }
        .frame(width: UIScreen.main.bounds.width)
        .gesture(DragGesture(minimumDistance: 1, coordinateSpace: .local)
                    .onChanged({ value in
            if value.translation.height >= 0 {
                dragYValue = dragYValue + value.translation.height
            }
            debugPrint("Drag : \(dragYValue)")
        })
                    .onEnded({ value in
            withAnimation {
                if dragYValue > 150 {
                    dragYValue = UIScreen.main.bounds.height
                    completion()
                } else {
                    dragYValue = 0
                }
            }
        })
        )
        .background(Color(UIColor.Theme.primaryContrast))
        .cornerRadius(radius: 28, corners: [.topLeft,.topRight])
        .offset(x: 0, y: dragYValue)
    }
}

struct BottomSheetComponentView: View {
    var completion:(() -> Void)
    var gender:Gender = .Female
    var body: some View {
        VStack{
            CrossView(completion: {})
            VStack(spacing: -60){
                ZStack{
                    let imageName:String = self.gender == .Female ? "femaleExpiringOnBoarding" : "maleExpiringOnBoarding"
                    let offsetY:CGFloat = self.gender == .Male ? 5 : 0
                    Group {
                        Image("half.iphone")
                        Image(imageName)
                            .offset(y: offsetY)
                    }
                }
                ZStack{
                    Color(UIColor.Theme.primaryContrast)
                        .padding(.horizontal, 30)
                        .padding(.top, 24)
                        .shadow(color: .black.opacity(0.2), radius: 10, x: 0, y: -10)
                    Image("expiryOnboardingBackground")
                        .scaledToFit()
                        .padding(.horizontal, 18)
                        .frame(width: UIScreen.main.bounds.width)
                    Color(UIColor.Theme.primaryContrast)
                        .padding(.top, 80)
                        .padding(.bottom, -20)
                }
            }
            .offset(y: 132)
            .frame(height: 240)
            VStack(alignment:.leading){
                TitleView()
                SubTitleView(completion: {
                    completion()
                })
            }
            .padding(EdgeInsets(top: 0, leading: 24, bottom: 0, trailing: 24))
            GradientCTA(title: "Show me my Requests", action: {})
            DisclaimerView(title: "You can change your Expiry preferences from your ",linkText: "Settings")
        }
    }
}

struct TitleView: View {
    var body: some View {
        Text("Requests expiring in 14 days! Respond now to boost your Partner search")
            .modifier(AccessibilityIDs(identifier: "labelTitleID", value: "Respond quickly & boost your Partner search", index: nil))
            .font(.system(size: 20, weight: .medium, design: .default))
            .foregroundColor(Color.init("primaryTextColor"))
            .padding(.bottom, 4)
            .multilineTextAlignment(.leading)
    }
}

struct SubTitleView: View {
    var completion:(() -> Void)
    var body: some View {
        VStack(alignment: .leading){
            Text("These Matches already like you and are waiting for your response. Respond to them before their Request expires.")
                .font(.system(size: 14, weight: .regular, design: .default))
                .foregroundColor(Color.init("secondaryTextColor"))
                .padding(.bottom, 2)
            Text("Once expired, you can re-connect with these Matches from Expired folder in your Inbox.")
                .font(.system(size: 14, weight: .regular, design: .default))
                .foregroundColor(Color.init("secondaryTextColor"))
        }
        .lineSpacing(1.1)
        .padding(.bottom, 18)
    }
}

struct CrossView: View {
    var completion: (()->Void)
    var body: some View {
        HStack{
            Spacer()
            Button {
                completion()
                debugPrint("Dismiss View")
            } label: {
                Image(systemName: "xmark")
                    .frame(width: 14,
                           height: 14,
                           alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                    .font(.system(size: 18,
                                  weight: .semibold,
                                  design: .default))
                    .foregroundColor(Color.init("lightGray"))
                    .padding(20)
            }
            .opacity(0)
        }
    }
}

struct LearnCloseView:View {
    var completion: (()->Void)
    var body: some View {
        CrossView {
            completion()
        }
    }
}

struct GradientCTA: View {
    
    var title:String
    var action: (()->Void)
    
    var body: some View {
        Button {
            action()
            debugPrint("\(title) Tapped")
        } label: {
            Text(title)
                .padding()
                .frame(width: UIScreen.main.bounds.width - 40)
                .background(
                    LinearGradient(gradient: Gradient(colors: [Color.init("gradientLightBlue"), Color.init("gradientDarkBlue")]), startPoint: .top, endPoint: .bottom)
                )
                .cornerRadius(54)
                .font(.system(size: 18, weight: .bold, design: .default))
                .foregroundColor(.white)
            
        }
        .padding(.bottom, 20)
        .shadow(color: .black.opacity(0.2), radius: 5, x: 0, y: 5)
    }
}

struct LearnMoreCTA: View {
    var title:String
    var action: (()->Void)
    var body: some View {
        GradientCTA(title: title,action: action)
    }
}

enum Gender: String {
    case Male = "male"
    case Female = "female"
}

struct BottomSheetBackgroundView: View {
    var gender:Gender = .Male
    var body: some View {
        let imageName:String = self.gender == .Female ? "femaleExpiringOnBoarding" : "maleExpiringOnBoarding"
        let offsetY:CGFloat = self.gender == .Male ? 5 : 0
        Color(.white)
            .cornerRadius(radius: 28, corners: [.topLeft,.topRight])
        Group {
            Image("half.iphone")
            Image(imageName)
                .offset(y: offsetY)
        }
        .offset(x: 0, y: -120)
        Group{
            Rectangle()
                .frame(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.width)
                .foregroundColor(.white)
                .shadow(radius: 20)
                .offset(x: 0, y: 90)
            Image("curve.background")
                .resizable()
                .frame(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.width)
                .offset(x: 0, y: 60)
        }
        .offset(x: 0, y: 88)
    }
}

struct DisclaimerView: View {
    
    var title:String
    var linkText:String
    
    var body: some View {
        Group{
            Text(title)
                .font(.system(size: 11, weight: .regular, design: .default))
                .italic()
                .foregroundColor(Color.init("lighterGrayTextColor")) +
                Text(linkText)
                .font(.system(size: 11, weight: .regular, design: .default))
                .italic()
                .foregroundColor(Color.init("lightBlueTextColor"))
        }
        .frame(width:UIScreen.main.bounds.width - 48)
        .padding(.bottom, 16)
        .onTapGesture {
            debugPrint("Settings Tapped")
        }
    }
}

struct LearnMoreView: View {
    var backgroundHidden:Bool
    var closeAction:(()->Void)
    var body: some View {
        ZStack(alignment:.bottom){
            LearnMoreTransparentBackgroundView(isHidden: backgroundHidden, action: closeAction)
            LearnMoreBottomSheetView {
                closeAction()
            }
        }
    }
}

struct LearnMoreDescriptionView: View {
    
    var expiringDays:Int
    
    var body: some View {
        let dayText = expiringDays > 1 ? "\(expiringDays) days" : "a day"
        Group{
            Text("Your Requests will expire after  ")
                .font(.system(size: 14, weight: .regular, design: .default))
                .foregroundColor(Color.init("secondaryTextColor")) +
                Text("\(dayText)")
                .font(.system(size: 14, weight: .semibold, design: .default))
                .foregroundColor(Color.init("secondaryTextColor")) +
                Text(", if not responded. However, you can re-connect with these Matches from Expired folder in your Inbox.")
                .font(.system(size: 14, weight: .regular, design: .default))
                .foregroundColor(Color.init("secondaryTextColor"))
        }
        .frame(width: UIScreen.main.bounds.width - 48)
        .padding(.bottom, 22)
        .lineSpacing(4)
    }
}

struct LearnMoreBottomSheetView: View {
    var closeAction:(()->Void)
    var body: some View {
        ZStack{
            Rectangle()
                .frame(width: UIScreen.main.bounds.width, height: 300, alignment: .top)
                .offset(x: 0, y: 80)
                .foregroundColor(.white)
            Color(.white)
                .cornerRadius(radius: 28, corners: [.topLeft,.topRight])
            VStack{
                LearnCloseView {
                    closeAction()
                }
            }
            VStack{
                LearnMoreDescriptionView(expiringDays: 14)
                LearnMoreCTA(title: "Ok, got it",action: closeAction)
            }
        }
        .frame(width: UIScreen.main.bounds.width, height: 222, alignment: .top)
    }
}

struct BottomSheet_ContentView_Previews: PreviewProvider {
    static var previews: some View {
        Group{
            ContentView()
//            BottomSheetView(completion: {})
//            BottomSheetBackgroundView()
//            BottomSheetComponentView(completion: {})
//            TitleView()
//            SubTitleView(completion: {})
//            CrossView(completion: {})
//            GradientCTA(title: "Show me Requests", action: {})
        }
    }
}

struct TextWithAccessibility: View {
    var title:String
    var id:String
    var index: Int?
    var body: some View {
        let postFix = index != nil ? "_\(index ?? 0)" : ""
        let indentifier = "\(id)\(postFix)"
        Text(title)
            .accessibility(value: Text(title))
            .accessibility(label: Text(indentifier))
            .accessibility(identifier: indentifier)
    }
}
