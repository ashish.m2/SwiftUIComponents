//
//  ConfirmationView.swift
//  ComponentCreation (iOS)
//
//  Created by Ashish Mankar on 09/12/21.
//

import SwiftUI

struct ConfirmationView: View {
    var title = "Do you want to send Accepts to all 13 Requests?"
    var closeAction:()->()
    var confirmAction:()->()
    @State var backgroundTapped: Bool = true
    var body: some View {
        ZStack(alignment:.bottom){
            TransparentBackgroundView()
                .onTapGesture {
                    withAnimation {
                        backgroundTapped = true
                    }
                    closeAction()
                }
            Confirmation(title: title,
                         closeAction: closeAction,
                         confirmAction: confirmAction)
                .offset(x: 0, y: backgroundTapped ? UIScreen.main.bounds.height : 0)
        }
        .onAppear{
            withAnimation{
                backgroundTapped = false
            }
        }
    }
}

struct CallToActionWithAnimation: View {
    var animatingAction:(_ animating : Bool)->()
    var confirmAction:()->()
    @State private var widthAnimationFlag: Bool = false
    @State private var animationCompleted: Bool = false
    @State private var buttonTapped: Bool = false
    @State private var tickAnimation: Bool = false
    @State private var showTickView: Bool = false
    @State private var completeAnimation: Bool = false
    var body: some View {
        ZStack(alignment: .center){
            if showTickView {
                Group{
                    Circle()
                        .foregroundColor(Color("buttonGreenColor"))
                    
                    Image(systemName: "checkmark")
                        .font(.system(size: 20, weight: .semibold, design: .default))
                        .foregroundColor(.white)
                    HStack{
                        Rectangle()
                            .foregroundColor(Color("buttonGreenColor"))
                            .offset(x: tickAnimation ? 40 : 0)
                            .clipShape(Circle())
                    }
                }
                .frame(width: 40,height: 40)
                .opacity(widthAnimationFlag ? 1 : 0)
            }
            Button {
                let duration:Double = 0.4
                buttonTapped = true
                debugPrint("Confirm Tapped")
                withAnimation (.linear(duration: duration)) {
                    widthAnimationFlag = true
                }
                DispatchQueue.main.asyncAfter(deadline: .now() + duration, execute: {
                    animationCompleted  = true
                    withAnimation (.linear(duration: duration)) {
                        tickAnimation = true
                    }
                    DispatchQueue.main.asyncAfter(deadline: .now() + duration, execute: {
                        completeAnimation = true
                        confirmAction()
                    })
                })
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                    showTickView = true
                }
                animatingAction(widthAnimationFlag)
            } label: {
                Text(widthAnimationFlag ? "" : "Confirm")
                    .padding(EdgeInsets(top: 12, leading: buttonTapped ? 20 : 52, bottom:12, trailing: buttonTapped ? 20 : 52))
                    .background(
                        LinearGradient(gradient: Gradient(colors: [Color.init(buttonTapped ? "buttonGreenColor" : "gradientLightBlue"), Color.init(buttonTapped ? "buttonGreenColor" : "gradientDarkBlue")]), startPoint: .top, endPoint: .bottom)
                    )
                    .cornerRadius(54)
                    .font(.system(size: 16,
                                  weight: .bold,
                                  design: .default))
                    .foregroundColor(.white)
            }
            .shadow(color: /*@START_MENU_TOKEN@*/.black/*@END_MENU_TOKEN@*/
                        .opacity(0.2),
                    radius:10,
                    x:0,
                    y: 10)
            .frame(width: widthAnimationFlag ? 40 : .infinity, height: 40)
            .opacity(animationCompleted ? 0 : 1)
        }
    }
}

struct CancelCallToAction: View {
    var widthAnimationFlag: Bool = false
    var closeAction:()->()
    var body: some View {
        Button {
            debugPrint("Cancel Tapped")
            closeAction()
        } label: {
            Text("Cancel")
                .font(.system(size: 16, weight: .medium, design: .default))
                .padding(EdgeInsets(top: 12, leading: 38, bottom: 12, trailing: 38))
                .foregroundColor(Color("secondaryTextColor"))
                .background(
                    Color("buttonGrayColor")
                        .cornerRadius(20)
                )
                .opacity(widthAnimationFlag ? 0 : 1)
        }
    }
}

struct ConfirmationTitleView: View {
    var title: String = ""
    var body: some View {
        Text(title)
            .font(.system(size: 18, weight: .medium, design: .default))
            .multilineTextAlignment(.center)
            .padding(EdgeInsets(top: 0, leading: 32, bottom: 0, trailing: 32))
            .foregroundColor(Color("primaryTextColor"))
    }
}

struct Confirmation: View {
    var title: String = ""
    var closeAction: () -> ()
    var confirmAction: () -> ()
    @State var closed: Bool = false
    @State private var widthAnimationFlag: Bool = false
    var body: some View {
        let safeArea:CGFloat = 50
        let height:CGFloat = 192
        let totalHeight = safeArea + height
        
        ZStack{
            let color = Color.white
            Rectangle()
                .foregroundColor(color)
                .offset(x: 0, y: safeArea)
            Rectangle()
                .foregroundColor(color)
                .cornerRadius(24)
            VStack{
                ConfirmationTitleView(title: title)
                HStack(spacing: 16){
                    CancelCallToAction(widthAnimationFlag: widthAnimationFlag, closeAction: {
                        withAnimation {
                            closed.toggle()
                        }
                        closeAction()
                    })
                    CallToActionWithAnimation { animating in
                        widthAnimationFlag = animating
                    } confirmAction: {
                        withAnimation {
                            closed.toggle()
                        }
                    }
                }
                .frame(width: UIScreen.main.bounds.width)
                .padding(EdgeInsets(top: 26, leading: 0, bottom: 0, trailing: 0))
            }
        }
        .frame(height:192)
        .offset(x: 0, y: closed ? totalHeight : 0)
    }
}

struct ConfirmationView_Previews: PreviewProvider {
    static var previews: some View {
        ConfirmationView(closeAction: {}, confirmAction: {},backgroundTapped: true)
    }
}
