//
//  ExpiringWidgetView.swift
//  ComponentCreation (iOS)
//
//  Created by Ashish Mankar on 30/11/21.
//

import SwiftUI

struct WidgetCallToAction: View {
    var title: String
    var body: some View {
        Button(action: {}, label: {
            Text(title)
                .font(.system(size: 13, weight: .medium, design: .default))
                .foregroundColor(Color("lightBlueTextColor"))
            Image("UploadLaterArrow")
                .resizable()
                .renderingMode(.template)
                .foregroundColor(Color("lightBlueTextColor"))
                .scaledToFit()
                .frame(width: 6, height: 8)
                .offset(x: -4, y: 0)
        })
        .modifier(AccessibilityIDs(identifier: "buttonWidget", value: title, index: nil))
        .padding(14)
    }
}

struct WidgetTitle: View {
    var title: String
    var subtitle: String
    var body: some View {
        VStack(alignment: .leading, spacing: 2){
            Text(title)
                .modifier(AccessibilityIDs(identifier: "labelWidgetTitle", value: title, index: nil))
                .font(.system(size: 13, weight: .medium, design: .default))
                .foregroundColor(Color("primaryTextColor"))
                .lineLimit(1)
                .minimumScaleFactor(0.01)
            if subtitle != "" {
                Text(subtitle)
                    .modifier(AccessibilityIDs(identifier: "labelWidgetSubtitle", value: subtitle, index: nil))
                    .font(.system(size: 11, weight: .regular, design: .default))
                    .foregroundColor(Color("redErrorTextColor"))
            }
        }
    }
}

struct ProfilePictures: View {
    var picturesName:[UIImage]
    var body: some View {
        let pictureSpacing:CGFloat = -48
        let frameWidth:CGFloat = CGFloat(16 + (picturesName.count * 16))
        HStack(spacing: pictureSpacing){
            ForEach(picturesName, id: \.self) { picture in
                //WebImage(url: URL(string: ""))
                Image(uiImage: picture)
                    .foregroundColor(.blue)
                    .frame(width:32)
            }
        }
        .frame(width: frameWidth, height: 32, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
        .padding(.init(top: 0, leading: 12, bottom: 0, trailing: 0))
    }
}

struct Widget_Previews: PreviewProvider {
    static var previews: some View {
        Group{
            ExpiringWidgetView()
        }
    }
}
