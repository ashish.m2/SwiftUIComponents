//
//  InboxSettingsView.swift
//  ComponentCreation (iOS)
//
//  Created by Ashish Mankar on 16/12/21.
//

import SwiftUI

struct InboxTransparentBackgroundView: View {
    var show:Bool = false
    var body: some View {
        if show {
            Color(.black)
                .opacity(0.6)
        }
    }
}

struct InboxSettings: View {
    
    var backAction:()->()
    var doneAction:(Int)->()
    var expiredAction:()->()
    @State var days:Int = 16
    @State private var showPicker:Bool = false
    var body: some View {
        let resetDays = days
        ZStack(alignment:.bottom){
            ZStack(alignment:.bottom){
                VStack(alignment:.leading){
                    CustomTopBar(doneAction: {
                        doneAction(days)
                    }, backAction: backAction)
                    InboxSettingsSeperator()
                    InboxSettingsComponentGroup(expiryDays: $days, dropDownAction: {
                        animatePicker()
                    }, expiredAction: expiredAction)
                    Spacer()
                }
            }
            PickerViewWithBackground(days: $days, resetDays: resetDays, showPicker: showPicker, cancelAction: {
                debugPrint("Cancel Tapped : \(resetDays)")
                animatePicker()
            }, doneAction: {
                debugPrint("Done Tapped : \(resetDays)")
                animatePicker()
            })
        }
        
    }
    
    func animatePicker(){
        withAnimation {
            showPicker.toggle()
        }
    }
}

struct InboxSettingsView_Previews: PreviewProvider {
    static var previews: some View {
        InboxSettings(backAction: {}, doneAction: {_ in},expiredAction: {})
    }
}

struct InboxSettingsComponentGroup: View {
    @Binding var expiryDays:Int
    var dropDownAction:()->()
    var expiredAction:()->()
    var body: some View {
        Group{
            InboxSettingsTitle()
            InboxSettingsDropDown(expiryDays: $expiryDays, dropDownAction: dropDownAction)
                .onTapGesture {
                    debugPrint("DropDown Tapped")
                }
            InboxSettingsFooter(expiredAction: expiredAction)
        }
        .padding(EdgeInsets(top: 0, leading: 16, bottom: 0, trailing: 16))
        ExpiredToolTip(width: 310)
    }
}

struct InboxSettingsTitle: View {
    var body: some View {
        Text("Choose the expiry preferences for your received Requests")
            .font(.system(size: 20, weight: .regular, design: .default))
            .foregroundColor(Color("titleTextColor"))
            .padding(EdgeInsets(top: 28, leading: 0, bottom: 28, trailing: 0))
            .multilineTextAlignment(.leading)
    }
}

struct InboxSettingsDropDown: View {
    @Binding var expiryDays:Int
    var dropDownAction:()->()
    var body: some View {
        VStack{
            Button(action: dropDownAction, label: {
                HStack{
                    Text("\(expiryDays) days")
                        .font(.system(size: 14, weight: .regular, design: .default))
                        .foregroundColor(Color("primaryTextColor"))
                    Spacer()
                    Image(systemName: "chevron.down")
                        .font(.system(size: 14, weight: .bold, design: .default))
                        .foregroundColor(Color("darkGray"))
                }
            })
            Color("seperatorColor")
                .frame(height:1)
        }
    }
}

struct InboxSettingsFooter: View {
    var expiredAction:()->()
    var body: some View {
        ZStack{
            Group{
                Text("If a Request expires before you respond, you can send that person a Connect Request from")
                    .foregroundColor(Color("tertiaryTextColor"))
                    +
                    Text(" Expired")
                    .foregroundColor(Color("lightBlueTextColor"))
                    +
                    Text(" folder in your Inbox.")
                    .foregroundColor(Color("tertiaryTextColor"))
            }
        }
        .font(.system(size: 14, weight: .regular, design: .default))
        .padding(EdgeInsets(top: 18, leading: 0, bottom: 0, trailing: 0))
        .lineSpacing(1.1)
        .onTapGesture{
            debugPrint("Expired Tapped")
        }
    }
}

struct InboxSettingsSeperator: View {
    var body: some View {
        Color(.black)
            .opacity(0.03)
            .shadow(color: .black, radius: 4, x: 2, y: 4)
            .frame(height:1)
    }
}

struct InboxSettingsNavigationBar: View {
    var backAction:()->()
    var doneAction:()->()
    var body: some View {
        ZStack{}
            .navigationBarItems(leading:
                                    HStack{
                                        Button(action: backAction, label: {
                                            Text("Cancel")
                                                .fontWeight(.regular)
                                                .foregroundColor(Color("lightBlueTextColor"))
                                        })
                                    }, trailing:
                                        HStack{
                                            Button(action: doneAction, label: {
                                                Text("Done")
                                                    .fontWeight(.bold)
                                                    .foregroundColor(Color("lightBlueTextColor"))
                                            })
                                        }
            )
            .navigationTitle("Inbox")
            .navigationBarColor(backgroundColor: .white, titleColor: UIColor(Color("secondaryTextColor")))
            .navigationBarTitleDisplayMode(.inline)
    }
}

struct ExpiryDaysPickerView: View {
    var pickerDoneAction:(Int)->()
    var pickerCancelAction:()->()
    @State var days:Int
    var resetDays:Int
    var body: some View {
        VStack(spacing:0){
            let expiryDays = (14...21)
            Rectangle()
                .opacity(0.01)
                .onTapGesture {
                    debugPrint("Background Tapped")
                    self.days = resetDays
                    pickerCancelAction()
                }
            HStack{
                Button(action: {
                    self.days = resetDays
                    pickerCancelAction()
                }, label: {
                    Text("Cancel")
                })
                Spacer()
                Text("Choose expiry preferences")
                Spacer()
                Button(action: {
                    pickerDoneAction(days)
                }, label: {
                    Text("Done")
                })
            }
            .padding(EdgeInsets(top: 20, leading: 12, bottom: 20, trailing: 12))
            .background(Color.white)
            Picker(selection: $days, label: Text("Picker"), content: {
                ForEach(expiryDays,id: \.self) {
                    Text("\($0) days").tag($0)
                }
            })
                .pickerStyle(.wheel)
            .frame(height:166)
            .background(Color.white)
        }
        
    }
}

struct PickerViewWithBackground: View {
    @Binding var days:Int
    var resetDays:Int
    var showPicker:Bool
    var cancelAction:()->()
    var doneAction:()->()
    var body: some View {
        let yAxis = showPicker ? 0 : UIScreen.main.bounds.height
        let reset = resetDays
        ZStack(alignment:.bottom){
            InboxTransparentBackgroundView(show: showPicker)
                .edgesIgnoringSafeArea(showPicker ? .top : .all)
                .onTapGesture {
                    self.days = reset
                    cancelAction()
                }
            ExpiryDaysPickerView(pickerDoneAction: {days in
                self.days = days
                doneAction()
            }, pickerCancelAction: {
                self.days = reset
                cancelAction()
            }, days: days, resetDays: resetDays)
            .offset(x: 0, y: yAxis)
        }
    }
}

struct CustomTopBar: View {
    var doneAction:()->()
    var backAction:()->()
    var body: some View {
        HStack {
            Button(action: {
                backAction()
            }, label: {
                Text("Cancel")
                    .font(.system(size: 16, weight: .regular, design: .default))
                    .foregroundColor(Color("lightBlueTextColor"))
                    .padding(EdgeInsets(top: 0, leading: 16, bottom: 0, trailing: 16))
            })
            Spacer()
            Text("Inbox")
                .font(.system(size: 16, weight: .medium, design: .default))
                .foregroundColor(Color("secondaryTextColor"))
            Spacer()
            Button(action: {
                doneAction()
            }, label: {
                Text("Done")
                    .font(.system(size: 16, weight: .semibold, design: .default))
                    .foregroundColor(Color("lightBlueTextColor"))
                    .padding(EdgeInsets(top: 0, leading: 16, bottom: 0, trailing: 16))
            })
        }
    }
}

struct ExpiredToolTip: View {
    var width: CGFloat
    var body: some View {
        VStack(spacing: 0){
            HStack{
                Spacer()
                Image("coachUpArrow")
                    .padding(EdgeInsets(top: 0, leading: 0, bottom: 0, trailing: 12))
            }
            ZStack{
                VStack {
                    Group{
                        Text("Request expiring ")
                            .font(.system(size: 14, weight: .regular, design: .default))
                            +
                            Text("Today. ")
                            .font(.system(size: 14, weight: .bold, design: .default))
                            +
                            Text("If it expires before you respond, you can re-connect with him from Expired folder in your Inbox.")
                            .font(.system(size: 14, weight: .regular, design: .default))
                    }
                    .padding(EdgeInsets(top: 20, leading: 20, bottom: 0, trailing: 20))
                    .foregroundColor(.white)
                    .lineSpacing(1.1)
                    HStack{
                        Spacer()
                        Button(action: {}, label: {
                            Text("Okay, got it!")
                        })
                        .padding(EdgeInsets(top: 20, leading: 20, bottom: 20, trailing: 20))
                    }
                }
            }
            .background(Color(.black))
            .cornerRadius(5)
        }
        .frame(width:width)
    }
}
