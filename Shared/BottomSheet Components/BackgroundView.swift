//
//  BackgroundView.swift
//  ComponentCreation (iOS)
//
//  Created by Ashish Mankar on 17/11/21.
//

import SwiftUI


struct TransparentBackgroundView: View {
    var body: some View {
        Color(.black)
            .opacity(0.6)
            .edgesIgnoringSafeArea(.all)
    }
}

struct LearnMoreTransparentBackgroundView: View {
    var isHidden:Bool
    var action:(()->Void)
    var body: some View {
        if isHidden {
            TransparentBackgroundView()
                .hidden()
        } else {
            TransparentBackgroundView()
                .onTapGesture {
                    action()
                }
        }
    }
}

struct Background_ContentView_Previews: PreviewProvider {
    static var previews: some View {
        Group{
            TransparentBackgroundView()
        }
    }
}
