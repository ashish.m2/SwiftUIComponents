//
//  PremiumPitchUIView.swift
//  ComponentCreation (iOS)
//
//  Created by Ashish Mankar on 29/12/21.
//

import SwiftUI

struct PremiumCrossView: View {
    var completion: (()->Void)
    var body: some View {
        HStack{
            Spacer()
            Button {
                completion()
                debugPrint("Dismiss View")
            } label: {
                Image(systemName: "xmark")
                    .frame(width: 14,
                           height: 14,
                           alignment: .center)
                    .font(.system(size: 18,
                                  weight: .medium,
                                  design: .default))
                    .foregroundColor(.white)
                    .padding(18)
            }
        }
    }
}

struct PremiumPitchTransparentBackgroundView: View {
    @Binding var showPremiumPitch:Bool
    var body: some View {
        
        Color(.black)
            .opacity(0.6)
            .edgesIgnoringSafeArea(showPremiumPitch ? .top : .all)
    }
}

struct PremiumPitchUIView: View {
    @State var showPremiumPitch:Bool = false
    var body: some View {
        ZStack(alignment:.bottom){
            let yAxis = showPremiumPitch ? 0 : UIScreen.main.bounds.height
            PremiumPitchTransparentBackgroundView(showPremiumPitch: $showPremiumPitch)
            VStack{
                PremiumCrossView(completion: {
                    withAnimation {
                        showPremiumPitch = false
                    }
                })
                AcceptSentProfiles()
                Text("Accept Sent!")
                    .foregroundColor(.white)
                    .font(.system(size: 16, weight: .medium, design: .default))
                    .padding(EdgeInsets(top: 8, leading: 0, bottom: 0, trailing: 0))
                Text("Why wait? Contact them directly")
                    .foregroundColor(.white)
                    .font(.system(size: 20, weight: .medium, design: .default))
                    .padding(EdgeInsets(top: 16, leading: 10, bottom: 16, trailing: 10))
                    .lineLimit(1)
                    .minimumScaleFactor(0.01)
                PremiumPitchOptions()
                CTAWithPlanDetails()
                
            }
            .frame(width: UIScreen.main.bounds.width)
            .background(
                GradientBackgroundView()
            )
            .onAppear{
                withAnimation {
                    showPremiumPitch = true
                }
            }
            .offset(x: 0, y: yAxis)
        }
        
    }
}

struct PremiumPitchUIView_Previews: PreviewProvider {
    static var previews: some View {
        PremiumPitchUIView()
    }
}

struct PremiumText: View {
    var body: some View {
        Text("Save upto ")
            .foregroundColor(Color("primaryTextColor"))
            .font(.system(size: 16, weight: .bold, design: .default))
            +
            Text("55")
            .foregroundColor(Color("greenTextColor"))
            .font(.system(size: 18, weight: .semibold, design: .default))
            +
            Text("%\n")
            .foregroundColor(Color("greenTextColor"))
            .font(.system(size: 18, weight: .medium, design: .default))
            +
            Text("on Premium Plans!")
            .foregroundColor(Color("primaryTextColor"))
            .font(.system(size: 16, weight: .bold, design: .default))
    }
}

struct LayerElement {
    var image:UIImage
    var title:String
    var lockImage:UIImage
}

struct PremiumContactOptionView: View {
    var layerElement:LayerElement
    let width = UIScreen.main.bounds.width - 100
    var body: some View {
        let title = layerElement.title
        let image = layerElement.image
        let lockImage = layerElement.lockImage
        HStack{
            HStack(spacing: 12){
                Image(uiImage: image)
                    .foregroundColor(Color.green)
                    .frame(width:14, height:14)
                Text(title)
                    .foregroundColor(Color("secondaryTextColor"))
                    .multilineTextAlignment(.leading)
                    .font(.system(size: 14, weight: .regular, design: .default))
            }
            Spacer()
            Image(uiImage: lockImage)
                .foregroundColor(.green)
                .frame(width:14, height:14)
        }
        .padding(EdgeInsets(top: 18, leading: 0, bottom: 18, trailing: 0))
        .frame(width: width)
    }
}

struct GradientBackgroundView: View {
    var body: some View {
        ZStack(alignment: .top) {
            Color(.white)
                .cornerRadius(radius: 30, corners: [.topLeft,.topRight])
            Image("bottomSheetBackground")
                .resizable()
                .frame(width: UIScreen.main.bounds.width,height: 356)
        }
    }
}

struct AcceptSentProfiles: View {
    var profiles = [UIImage(),UIImage(),UIImage(),UIImage(),UIImage(),UIImage()]
    private var counter = 0
    var body: some View {
        HStack(spacing: -18){
            let count = profiles.count > 4 ? profiles.count - 3 : 0
            let counter = profiles.count > 3 ? 4 : profiles.count
            ForEach(0..<counter) { index in
                let shodowRadius:CGFloat = index == 3 && profiles.count > 4 ? 2 : 10
                let flag:Bool = (index == 3 && profiles.count > 4)
                ZStack{
                    Circle()
                        .strokeBorder(Color.white,lineWidth: 1)
                        .foregroundColor(.black.opacity(0.5))
                    if flag {
                        Text("+\(count)")
                            .foregroundColor(.white)
                            .font(.system(size: 14, weight: .bold, design: .default))
                    } else {
                        Image("ContextualSilhoutte_Male")
                            .resizable()
                            .frame(width: 58,height: 58)
                    }
                }
                .cornerRadius(30)
                .frame(width:60,height:60)
                .shadow(color: .black
                            .opacity(0.3),
                        radius: shodowRadius, x: 1, y: 4)
                .zIndex(Double(-index) * 0.2)
            }
            
        }
    }
}

struct PremiumPitchOptions: View {
    let lockImage = UIImage(named: "layer-lock") ?? UIImage()
    var body: some View {
        ZStack {
            let layerElements:[LayerElement] = [
                LayerElement(image: UIImage(named:"call_blue") ?? UIImage(), title: "View Contact Numbers", lockImage: lockImage),
                LayerElement(image: UIImage(named:"meetAV_blue") ?? UIImage(), title: "Meet them over video call", lockImage: lockImage),
                LayerElement(image: UIImage(named:"whatsapp-icon") ?? UIImage(), title: "Chat on WhatsApp", lockImage: lockImage),
                LayerElement(image: UIImage(named:"chat-bubble") ?? UIImage(), title: "Message via Shaadi Chat", lockImage: lockImage),
                LayerElement(image: UIImage(named:"LoginEmailBlue") ?? UIImage(), title: "Contact them over email", lockImage: lockImage)
            ]
            let width = UIScreen.main.bounds.width - 90
            VStack(spacing: 0){
                let lastTitle = layerElements.last?.title ?? ""
                ForEach(layerElements,id:\.title) { layerElement in
                    PremiumContactOptionView(layerElement: layerElement)
                    if layerElement.title != lastTitle {
                        Color("seperatorColor")
                            .frame(width: width,height: 1)
                    }
                }
            }
        }
        .frame(width: UIScreen.main.bounds.width - 60)
        .background(Color(.white)
                        .cornerRadius(6)
                        .shadow(color: /*@START_MENU_TOKEN@*/.black/*@END_MENU_TOKEN@*/.opacity(0.2), radius: /*@START_MENU_TOKEN@*/10/*@END_MENU_TOKEN@*/, x: /*@START_MENU_TOKEN@*/0.0/*@END_MENU_TOKEN@*/, y: /*@START_MENU_TOKEN@*/0.0/*@END_MENU_TOKEN@*/))
    }
}

struct CTAWithPlanDetails: View {
    var body: some View {
        HStack(spacing: 16){
            let title = "View Plans"
            Group {
                PremiumText()
            }
            Button {
                debugPrint("\(title) Tapped")
            } label: {
                Text(title)
                    .padding(EdgeInsets(top: 10, leading: 24, bottom: 10, trailing: 24))
                    .background(
                        LinearGradient(gradient: Gradient(colors: [Color.init("gradientLightBlue"), Color.init("gradientDarkBlue")]), startPoint: .top, endPoint: .bottom)
                        
                    )
                    .cornerRadius(54)
                    .font(.system(size: 18, weight: .bold, design: .default))
                    .foregroundColor(.white)
            }
            .shadow(color: Color("gradientLightBlue")
                        .opacity(0.4),
                    radius: 10,
                    x: 4,
                    y: 8)
        }
        .padding(EdgeInsets(top: 20, leading: 0, bottom: 32, trailing: 0))
    }
}
