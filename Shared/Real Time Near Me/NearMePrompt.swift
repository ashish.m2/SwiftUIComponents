//
//  NearMePrompt.swift
//  ComponentCreation (iOS)
//
//  Created by Ashish Mankar on 12/04/22.
//

import SwiftUI


struct NearMePrompt: View {
    @State var locations:[Location] = [Location(title: "Old Location", subtitle: "Mumbai", image: "hollow_location_mark",isSelected: false),Location(title: "New Location", subtitle: "Bengaluru", image: "solid_location_mark",isSelected: true)]
    var closeAction: () -> ()
    var updateAction: () -> ()
    @State var dragYValue:CGFloat = 0
    @State var updateLocation:Bool = false
    var body: some View {
        ZStack(alignment: .bottom){
            Color(.black)
                .opacity(0.5)
                .edgesIgnoringSafeArea(dragYValue == 0 ? .top : .all)
                .onTapGesture {
                    withAnimation {
                        dragYValue = UIScreen.main.bounds.height
                    }
                }
            VStack{
                Spacer()
                ProgressView()
                Spacer()
            }
            .opacity(updateLocation ? 1 : 0)
            UpdateChipView()
                .opacity(updateLocation ? 1 : 0)
            NearMeView(locations: locations,closeAction: {},updateAction: {
                withAnimation {
                    updateLocation = true
                }
            },dragYValue: $dragYValue)
        }
        
    }
}

struct NearMePrompt_Previews: PreviewProvider {
    static var previews: some View {
        NearMePrompt(closeAction: {}, updateAction: {})
    }
}

struct LocationButton: View {
    var location: Location
    var action: (Location) -> ()
    var body: some View {
        Button(action: {
            self.action(self.location)
        }, label: {
            let image = self.location.image
            let selectedColor = self.location.isSelected ? UIColor.Accent.primary : UIColor.Neutrals.divider
            let titleTextColor = self.location.isSelected ? UIColor.Accent.primary : UIColor.Neutrals.secondary
            let locationTextColor = self.location.isSelected ? UIColor.Accent.primary : UIColor.Other.Gray.light
            ZStack{
                VStack{
                    Image(image)
                    Text(self.location.title)
                        .foregroundColor(Color(titleTextColor))
                        .font(.system(size: CGFloat().getFontSize(12), weight: .regular, design: .default))
                        .padding(.top, 8)
                        .padding(.bottom, 2)
                    Text(self.location.subtitle)
                        .foregroundColor(Color(locationTextColor))
                        .font(.system(size: CGFloat().getFontSize(12), weight: .bold, design: .default))
                }
            }
            .frame(width: 140, height: 120)
            .cornerRadius(12)
            .overlay(
                RoundedRectangle(cornerRadius: 12)
                    .stroke(Color(selectedColor), lineWidth: 1)
            )
        })
    }
}

struct LocationChoice: View {
    @State var locations:[Location]
    var body: some View {
        HStack(spacing: 16) {
            ForEach(locations ,id: \.id) { location in
                LocationButton(location: location) { location in
                    self.locations = self.locations.map { loc in
                        var local = loc
                        local.isSelected = false
                        local.image = "hollow_location_mark"
                        if loc.id == location.id {
                            local.isSelected = true
                            local.image = "solid_location_mark"
                        }
                        return local
                    }
                }
            }
        }
    }
}

struct Location {
    let id = UUID()
    var title:String = ""
    var subtitle:String = ""
    var image:String = ""
    var isSelected:Bool = false
}

struct NearMeView: View {
    @State var locations:[Location]
    var closeAction: () -> ()
    var updateAction: () -> ()
    @Binding var dragYValue:CGFloat
    var body: some View {
        VStack {
            ZStack{
                HStack {
                    Spacer()
                    Button(action: {
                        closeAction()
                        withAnimation {
                            dragYValue = UIScreen.main.bounds.height
                        }
                    }, label: {
                        Image("close_comparison")
                    })
                    Spacer()
                        .frame(width: 20)
                }
                Image("nearme_mark")
                    .background(Color(UIColor.Theme.primaryContrast).clipShape(Circle()))
                    .offset(y: -36)
            }
            Text("You've logged in from a city different from the one registered with us")
                .foregroundColor(Color(UIColor.Other.Gray.light))
                .font(.system(size: CGFloat().getFontSize(18), weight: .bold, design: .default))
                .multilineTextAlignment(.center)
                .padding(.horizontal, 18)
            Spacer()
                .frame(height: 10)
            Text("Would you like to update your Profile location?")
                .foregroundColor(Color(UIColor.Neutrals.secondary))
                .font(.system(size: CGFloat().getFontSize(14), weight: .regular, design: .default))
                .lineLimit(1)
                .minimumScaleFactor(0.1)
                .padding(.horizontal, 30)
            Spacer()
                .frame(height: 32)
            LocationChoice(locations: locations)
            Spacer()
                .frame(height: 32)
            HStack{
                Spacer()
                    .frame(width: 36)
                Button(action: {
                    updateAction()
                    withAnimation {
                        dragYValue = UIScreen.main.bounds.height
                    }
                }, label: {
                    HStack{
                        Spacer()
                        Text("Update My Location")
                            .foregroundColor(Color(UIColor.Theme.primaryContrast))
                            .font(.system(size: CGFloat().getFontSize(18), weight: .bold, design: .default))
                            .padding(.vertical, 14)
                        Spacer()
                    }
                    .background(LinearGradient(colors: [Color(UIColor.Other.gradientBlue),Color(UIColor.Accent.primary)], startPoint: .top, endPoint: .bottom))
                    .cornerRadius(24)
                })
                    .shadow(color: Color(UIColor.Accent.primary), radius: 4, x: 0, y: 2)
                Spacer()
                    .frame(width: 36)
            }
            Spacer()
                .frame(height:40)
        }
        .background(Color(UIColor.Theme.primaryContrast)
                        .cornerRadius(radius: 24, corners: [.topLeft,.topRight]))
        .offset(y: dragYValue)
        .gesture(DragGesture(minimumDistance: 1, coordinateSpace: .local)
                    .onChanged({ value in
            if value.translation.height >= 0 {
                dragYValue = value.translation.height
            }
            debugPrint("Drag : \(dragYValue)")
        })
                    .onEnded({ value in
            withAnimation {
                if dragYValue > 150 {
                    dragYValue = UIScreen.main.bounds.height
                    closeAction()
                } else {
                    dragYValue = 0
                }
            }
        })
        )
    }
}

struct UpdateChipView: View {
    var body: some View {
        VStack{
            ZStack{
                Text("Your Location is now updated")
                    .foregroundColor(Color(UIColor.Other.darkGreenPremium))
                    .font(.system(size: CGFloat().getFontSize(14), weight: .bold, design: .default))
                    .padding(.horizontal, 20)
                    .padding(.vertical, 10)
            }
            .background(
                Color(UIColor.Theme.primaryContrast)
                    .cornerRadius(18)
                    .shadow(color: .black
                                .opacity(0.5),
                            radius: 4,
                            x: 0,
                            y: 2)
            )
            Spacer()
                .frame(height: 34)
        }
    }
}
