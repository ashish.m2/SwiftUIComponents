//
//  RecommendationToolTipView.swift
//  ComponentCreation (iOS)
//
//  Created by Ashish Mankar on 15/04/22.
//

import SwiftUI

struct RecommendationToolTipView: View {
    var tooltipAction: () -> ()
    var actionOutSideToolTip: () -> ()
    var body: some View {
        VStack{
            HStack(spacing: 0){
                Spacer()
                ZStack{
                    Group{
                        Text("There’s something new in ")
                            .foregroundColor(
                                Color(
                                    UIColor
                                        .Theme
                                        .primaryContrast
                                )
                            )
                            .font(.system(
                                size: CGFloat()
                                    .getFontSize(14),
                                weight: .medium,
                                design: .default))
                        +
                        Text("Near Me")
                            .font(.system(
                                size: CGFloat()
                                    .getFontSize(14),
                                weight: .medium,
                                design: .default))
                            .underline()
                            .foregroundColor(Color(
                                UIColor
                                    .Theme
                                    .primaryContrast)
                            )
                        +
                        Text(" 👉")
                            .foregroundColor(Color(
                                UIColor
                                    .Theme
                                    .primaryContrast)
                            )
                            .font(.system(
                                size: CGFloat().getFontSize(14),
                                weight: .medium,
                                design: .default))
                    }
                    .padding(16)
                }
                .background(
                    Color(
                        UIColor
                            .Other
                            .blackShade
                    )
                        .cornerRadius(6)
                )
                .shadow(
                    color: .black
                        .opacity(0.4)
                    ,
                    radius: 7,
                    x: 0,
                    y: 1)
                Image("coachUpArrow")
                    .rotationEffect(Angle(degrees: 90))
                    .offset(x: -3)
            }
            .onTapGesture {
                tooltipAction()
            }
            Spacer()
        }
        .onTapGesture {
            actionOutSideToolTip()
        }
    }
}

struct RecommendationToolTipView_Previews: PreviewProvider {
    static var previews: some View {
        RecommendationToolTipView(tooltipAction: {}, actionOutSideToolTip: {})
    }
}
