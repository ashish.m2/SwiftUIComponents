//
//  NearMePermissionView.swift
//  ComponentCreation (iOS)
//
//  Created by Ashish Mankar on 08/04/22.
//

import SwiftUI

struct NearMeViewModel{
    private func getFloatConstant(fontSize: CGFloat) -> CGFloat {
        return UIScreen.main.bounds.width/fontSize
    }
    
    func getFontSize(fontSize: CGFloat) -> CGFloat {
        return CGFloat(Int(UIScreen.main.bounds.width/self.getFloatConstant(fontSize: fontSize)))
    }
    
    func getGradientOuterCircleOffsetY() -> CGFloat {
        return UIScreen.main.bounds.width / 3.125
    }
    
    func getGradientOuterCircleDiameter() -> CGFloat {
        return UIScreen.main.bounds.width + 20
    }
    
    func getGradientInnerCircleDiameter() -> CGFloat {
        return UIScreen.main.bounds.width - 54 - 24
    }
    
    func getGradientViewWidth() -> CGFloat {
        return UIScreen.main.bounds.width + 2
    }
    
    func getGradientViewHeight() -> CGFloat {
        return UIScreen.main.bounds.width/1.55
    }
    
    func getGradientFirstDot() -> CGFloat {
        return UIScreen.main.bounds.width / 10.9
    }
}

struct NearMePermissionView: View {
    var viewModel:NearMeViewModel = NearMeViewModel()
    var body: some View {
        VStack{
            PermissionTitle()
            PermissionGradient()
            PermissionFooterElements()
        }
    }
}

struct NearMePermissionView_Previews: PreviewProvider {
    static var previews: some View {
        NearMePermissionView()
    }
}

struct PermissionTitle: View {
    var viewModel:NearMeViewModel = NearMeViewModel()
    var body: some View {
        Group{
            Spacer()
                .frame(height: 24)
            Text("Experience the new Near Me")
                .foregroundColor(Color(UIColor.Other.Gray.light))
                .font(.system(size: self.viewModel.getFontSize(fontSize: 24), weight: .bold, design: .default))
                .italic()
                .lineLimit(1)
                .minimumScaleFactor(0.5)
                .padding(.horizontal, 20)
            Spacer()
                .frame(height: 8)
            Text("See profiles around the whole world and near you. Explore. Connect. Match")
                .foregroundColor(Color(UIColor.Neutrals.tertiary))
                .font(.system(size: self.viewModel.getFontSize(fontSize: 14), weight: .regular, design: .default))
                .padding(.horizontal, 20)
                .multilineTextAlignment(.center)
        }
    }
}

struct PermissionFooterElements: View {
    var viewModel:NearMeViewModel = NearMeViewModel()
    var body: some View {
        Group {
            Text("To see relevant matches near you, allow Shaadi.com to access your approximate location")
                .foregroundColor(Color(UIColor.Neutrals.primary))
                .font(.system(size: 14, weight: .medium, design: .default))
                .multilineTextAlignment(.center)
                .padding(.horizontal, 20)
            Spacer()
                .frame(height: 34)
            Button(action: {}, label: {
                ZStack{
                    Text("Allow Access")
                        .foregroundColor(Color(UIColor.Theme.primaryContrast))
                        .font(.system(size: self.viewModel.getFontSize(fontSize: 14), weight: .bold, design: .default))
                        .padding(.vertical, 12)
                }
                .padding(.horizontal, 66)
                .background(LinearGradient(colors: [Color(UIColor.Other.gradientBlue), Color(UIColor.Accent.primary)], startPoint: .top, endPoint: .bottom))
                .cornerRadius(20)
                .shadow(color: Color(UIColor.Other.gradientBlue), radius: 4, x: 0, y: 2)
            })
            Spacer()
                .frame(height: 12)
            Button(action: {}, label: {
                Text("Not Now")
                    .foregroundColor(Color(UIColor.Other.actionStatus))
                    .font(.system(size: self.viewModel.getFontSize(fontSize: 14), weight: .regular, design: .default))
            })
        }
    }
}

struct PermissionGradient: View {
    var viewModel:NearMeViewModel = NearMeViewModel()
    var body: some View {
        Group {
            Spacer()
                .frame(height: 30)
            ZStack{
                Image("near_me_radar")
                    .resizable()
                    .frame(width: self.viewModel.getGradientViewWidth(),height: self.viewModel.getGradientViewHeight())
                Circle()
                    .trim(from: 0.5, to: 1)
                    .stroke(lineWidth: 1.5)
                    .foregroundColor(Color(UIColor.Theme.primaryContrast))
                    .frame(width: self.viewModel.getGradientOuterCircleDiameter() ,height: self.viewModel.getGradientOuterCircleDiameter())
                    .offset(y: self.viewModel.getGradientOuterCircleOffsetY())
                Circle()
                    .trim(from: 0.5, to: 1)
                    .stroke(lineWidth: 2)
                    .foregroundColor(Color(UIColor.Theme.primaryContrast))
                    .frame(width: self.viewModel.getGradientInnerCircleDiameter() ,height: self.viewModel.getGradientInnerCircleDiameter())
                    .offset(y: self.viewModel.getGradientOuterCircleOffsetY())
                HStack{
                    Spacer()
                        .frame(width: self.viewModel.getGradientFirstDot())
                    Circle()
                        .foregroundColor(Color(UIColor.Theme.primaryContrast))
                        .frame(width: 12, height: 12)
                        .shadow(color: Color(UIColor.Theme.primaryContrast).opacity(0.5), radius: 8, x: 0, y: 0)
                    Spacer()
                }
            }
            .frame(width: self.viewModel.getGradientViewWidth(),height: self.viewModel.getGradientViewHeight())
            Spacer()
                .frame(height: 34)
        }
    }
    
    func getCircularShape() -> Path {
        var path = Path()
        path.move(to: CGPoint(x: 0, y: 0))
        path.addQuadCurve(to: CGPoint(x: UIScreen.main.bounds.width, y: 0), control: CGPoint(x: UIScreen.main.bounds.width/2, y: -self.viewModel.getGradientViewHeight()))
        return path
    }
}
