//
//  NearMePermissionDeniedView.swift
//  ComponentCreation (iOS)
//
//  Created by Ashish Mankar on 28/04/22.
//

import SwiftUI

#if DEBUG
struct NearMePermissionDeniedView_Previews: PreviewProvider {
    static var previews: some View {
        NearMePermissionDeniedView()
    }
}
#endif

struct LocationNote: Identifiable {
    var id:UUID = UUID()
    var image: String
    var note: String
}

class NearMePermissionDeniedViewModel: ObservableObject {
    
    @Published var dismissed:Bool = true
    
    func getCloseImage() -> String {
        return "close_comparison"
    }
    
    func getWarningImage() -> String {
        return "warning"
    }
    
    func getTitle() -> String {
        return "Your Location Access is disabled"
    }
    
    func getSubtitle() -> String {
        return "Allow Location Access to see relevant Matches"
    }
    
    func getFooter() -> String {
        return "You will be redirected to Settings screen"
    }
    
    func getButtonCaption() -> String {
        return "Allow Location Access"
    }
    
    func getNotes() -> [LocationNote] {
        return [
            LocationNote(image: "NotificationTick", note: "Your location is safe with us"),
            LocationNote(image: "NotificationTick", note: "We don’t share your location details"),
            LocationNote(image: "NotificationTick", note: "Search for profiles in any location!"),
            LocationNote(image: "NotificationTick", note: "Match with like-minded people near you")
        ]
    }
}

struct NearMePermissionDeniedView: View {
    @StateObject private var viewModel: NearMePermissionDeniedViewModel = NearMePermissionDeniedViewModel()
    var body: some View {
        ZStack{
            Color(.black)
                .opacity(0.6)
                .ignoresSafeArea()
            VStack{
                Spacer()
                ZStack{
                    VStack(spacing: 0){
                        HStack{
                            Spacer()
                            Button(action: {
                                self.viewModel.dismissed.toggle()
                            }, label: {
                                Image("close_comparison")
                                    .resizable()
                                    .frame(width: CGFloat().getAspectSize(12), height: CGFloat().getAspectSize(12))
                                    .padding(EdgeInsets(top: CGFloat().getAspectSize(16), leading: CGFloat().getAspectSize(16), bottom: CGFloat().getAspectSize(4), trailing: CGFloat().getAspectSize(16)))
                            })
                        }
                        Image("warning-circle")
                            .resizable()
                            .frame(width: CGFloat().getAspectSize(48),height: CGFloat().getAspectSize(48))
                        TitleSubTitleView(viewModel: viewModel)
                        VStack(alignment: .leading,spacing: 14){
                            NearMePointView(title: "Your location is safe with us")
                            NearMePointView(title: "We don’t share your location details")
                            NearMePointView(title: "Search for profiles in any location!")
                            NearMePointView(title: "Match with like-minded people near you")
                        }
                        Spacer()
                            .frame(height: 32)
                        HStack{
                            Spacer()
                                .frame(width: CGFloat().getAspectSize(34))
                            Button(action: {}, label: {
                                ZStack{
                                    HStack{
                                        Spacer()
                                        Text(self.viewModel.getButtonCaption())
                                            .font(.system(size: CGFloat().getFontSize(18), weight: .bold, design: .default))
                                            .foregroundColor(Color(UIColor.Theme.primaryContrast))
                                            .padding(.vertical, 12)
                                        Spacer()
                                    }
                                }
                                .background(
                                    LinearGradient(colors: [Color(UIColor.Other.gradientBlue),Color(UIColor.Gradient.bottom)], startPoint: .top, endPoint: .bottom)
                                )
                                .cornerRadius(26)
                            })
                                .shadow(color: Color(UIColor.Other.gradientBlue).opacity(0.4), radius: 10, x: 0, y: 4)
                            Spacer()
                                .frame(width: CGFloat().getAspectSize(34))
                        }
                        Spacer()
                            .frame(height: 26)
                        Text(self.viewModel.getFooter())
                            .foregroundColor(Color(UIColor.Neutrals.tertiary))
                            .font(.system(size: CGFloat().getFontSize(12), weight: .regular, design: .default))
                            .multilineTextAlignment(.center)
                            .padding(.horizontal, 24)
                        Spacer()
                            .frame(height: 16)
                    }
                    //.animation(Animation.linear(duration: 0.2))
                }
                .background(
                    Color(UIColor.Theme.primaryContrast)
                        .cornerRadius(radius: 24, corners: [.topLeft, .topRight])
                        .edgesIgnoringSafeArea(.bottom)
                )
                .offset(y: self.viewModel.dismissed ? UIScreen.main.bounds.height : 0)
                //.animation(Animation.linear(duration: 0.5))
            }
        }
        .onTapGesture(perform: {
            self.viewModel.dismissed.toggle()
        })
        .onAppear{
            self.viewModel.dismissed = false
        }
    }
}

struct TitleSubTitleView: View {
    var viewModel: NearMePermissionDeniedViewModel
    var body: some View {
        VStack(spacing: 0){
            Spacer()
                .frame(height: 16)
            Text(self.viewModel.getTitle())
                .font(.system(size: CGFloat().getFontSize(18), weight: .bold, design: .default))
                .foregroundColor(Color(UIColor.Other.Gray.light))
                .padding(.horizontal, 18)
            Spacer()
                .frame(height: 4)
            Text(self.viewModel.getSubtitle())
                .font(.system(size: CGFloat().getFontSize(14), weight: .regular, design: .default))
                .foregroundColor(Color(UIColor.Neutrals.tertiary))
            Spacer()
                .frame(height: 26)
        }
    }
}

struct NearMePointView: View {
    var title: String = ""
    var body: some View {
        HStack{
            Spacer()
                .frame(width: 64)
            Image("NotificationTick")
                .resizable()
                .frame(width: 15, height: 14)
            Spacer()
                .frame(width: 10)
            Text(title)
                .font(.system(size: CGFloat().getFontSize(12), weight: .medium, design: .default))
                .foregroundColor(Color(UIColor.Other.actionStatus))
                .padding(.trailing, 54)
            Spacer()
        }
    }
}
