//
//  NearMeListingFooter.swift
//  ComponentCreation (iOS)
//
//  Created by Ashish Mankar on 05/05/22.
//

import SwiftUI

struct NearMeListingFooter_Previews: PreviewProvider {
    static var previews: some View {
        NearMeListingFooter(action: {})
    }
}

struct NearMeListingFooter: View {
    var action: () -> ()
    @State private var opcaity:CGFloat = 0
    var body: some View {
        ZStack{
            Color(UIColor.PremiumTab.background)
            ZStack{
                Color(UIColor.Theme.primaryContrast)
                VStack{
                    Spacer()
                    Text("That’s all we have in Near me. Continue searching for your perfect partner.")
                        .padding(.horizontal, 24)
                        .multilineTextAlignment(.center)
                        .font(.system(size: CGFloat().getFontSize(14), weight: .medium, design: .default))
                        .foregroundColor(Color(UIColor.Neutrals.primary))
                    Spacer()
                        .frame(height: 26)
                    BlueGradientButton(title: "Go to My Matches", horizontalPadding: 40,action: action)
                    Spacer()
                }
            }
            .cornerRadius(12)
            .padding(8)
            .shadow(color: .white.opacity(0.5), radius: 8, x: -4, y: -4)
        }
        .overlay(
            RoundedRectangle(cornerRadius: 12)
                .stroke(Color(UIColor.Neutrals.pendingNotification),lineWidth: 1)
        )
        .cornerRadius(12)
        .padding(.horizontal, 8)
        .padding(.vertical, 30)
        .opacity(self.opcaity)
        .animation(Animation.linear(duration: 0.5))
        .onAppear {
            self.opcaity = 1
        }
    }
}

struct BlueGradientButton: View {
    var title: String
    var horizontalPadding: CGFloat = 40
    var action: () -> ()
    var body: some View {
        HStack{
            Spacer()
                .frame(width: horizontalPadding)
            Button(action: {}, label: {
                HStack{
                    Spacer()
                    Text(title)
                        .foregroundColor(Color(UIColor.Theme.primaryContrast))
                        .font(.system(size: CGFloat().getFontSize(14), weight: .bold, design: .default))
                        .padding(.vertical, 12)
                        
                    Spacer()
                }
                .background(
                    LinearGradient(colors: [Color(UIColor.Other.gradientBlue),Color(UIColor.Gradient.bottom)], startPoint: .top, endPoint: .bottom)
                )
                .cornerRadius(20)
            })
                .shadow(color: Color(UIColor.Other.gradientBlue).opacity(0.4), radius: 10, x: 0, y: 4)
            Spacer()
                .frame(width: horizontalPadding)
        }
    }
}
