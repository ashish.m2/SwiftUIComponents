//
//  NearMeLocationSearchView.swift
//  ComponentCreation (iOS)
//
//  Created by Neha Yadav on 29/04/22.
//

import SwiftUI
import MapKit
import Combine

//MARK: - Views
struct NearMeLocationSearchBarView: View {
    
    @StateObject var viewModel: NearMeLocationSearchViewModel = NearMeLocationSearchViewModel()
    @StateObject private var locationService = LocationSearchService()
    
    var body: some View {
        VStack {
            HStack {
                Spacer()
                ZStack {
                    HStack {
                        Spacer()
                        NearMeSearchBarLocationButton(viewModel: viewModel)
                        Spacer()
                        Divider()
                            .foregroundColor(Color(viewModel.getDividerColor()))
                        Spacer()
                        NearMeSearchBarTextfieldView(locationService: locationService, viewModel: viewModel)
                        Spacer()
                    }
                }
                .foregroundColor(Color.clear)
                .overlay(viewModel.isEditingSearchQuery ? RoundedRectangle(cornerRadius: 40.0).stroke(Color(viewModel.getSearchViewActiveColor()), lineWidth: 1) : RoundedRectangle(cornerRadius: 40.0).stroke(Color(viewModel.getSearchViewInactiveColor()), lineWidth: 1)

                )
                Spacer()
                NearMeSearchBarFilterButtonView(viewModel: viewModel)
                Spacer()
            }
            .frame(width: UIScreen.main.bounds.width, height: viewModel.getViewHeight(), alignment: .top)
            NearMeLocationListView(viewModel: viewModel, locationService: locationService)
        }
    }
}

struct NearMeSearchBarLocationButton: View {
    
    @StateObject var viewModel: NearMeLocationSearchViewModel
    @State private var manuallyEnteredLocation = false

    var body: some View {
        Button(action: {
            viewModel.isManuallyEnteredLocation.toggle()
            //TODO: Fetch current location
        }) {
            Image(viewModel.isManuallyEnteredLocation ? viewModel.getManualLocationImage() : viewModel.getCurrentLocationMarkImage())
                .renderingMode(.original)
                .padding()
                .frame(width: 30.0, height: 30.0, alignment: .center)
        }
        .frame(width: viewModel.getViewHeight() * 0.7, height: viewModel.getViewHeight() * 0.7, alignment: .center)
    }
}

struct NearMeSearchBarTextfieldView: View {
    @StateObject var locationService: LocationSearchService
    var viewModel: NearMeLocationSearchViewModel

    var body: some View {
        TextField("Enter a location", text: $locationService.searchQuery, onEditingChanged: { isEditing in
            if isEditing {
                viewModel.isEditingSearchQuery = true
            } else {
                viewModel.isEditingSearchQuery = false
            }
        }, onCommit: {
            viewModel.isEditingSearchQuery = false
        })
            .foregroundColor(Color(UIColor.Neutrals.primary))
            .accentColor(Color(UIColor.Theme.primary))
    }
}

struct NearMeSearchBarFilterButtonView: View {
    
    var viewModel: NearMeLocationSearchViewModel
    
    var body: some View {
        Button(action: {
            debugPrint("Button tapped")
            //TODO: Add filter action
        }) {
            Image(viewModel.getFilterImage())
                .renderingMode(.original)
                .resizable()
                .frame(width: 15, height: 15, alignment: .center)
        }
        .frame(width: viewModel.getViewHeight(), height: viewModel.getViewHeight(), alignment: .center)
        .background(Color(UIColor.Neutrals.background))
        .clipShape(Circle())
    }
}

struct NearMeLocationListView: View {

    var viewModel: NearMeLocationSearchViewModel
    @StateObject var locationService: LocationSearchService
    
    var body: some View {
        List {
            if viewModel.isEditingSearchQuery {
            Section {
                Button {
                    debugPrint("Switch to current/reg location")
                    //TODO: Switch to current/ reg location
                } label: {
                    HStack {
                        Image(viewModel.getCurrentLocationListImage())
                            .renderingMode(.original)
                        //TODO: Update location text
                        Text("Switch back to my location - Mumbai")
                            .foregroundColor(Color(UIColor.Neutrals.primary))
                            .font(.system(size: 14, weight: .medium, design: .default))
                    }
                }
            }
        }
            ForEach(locationService.locationSearchResults, id: \.self) { location in
                Button(action: {
                    debugPrint("===========")
                    debugPrint("Selected Location: \(location)")
                    self.viewModel.selectedLocation = location
                    //TODO: Call fetchLocationDetail() to get coordinates for selected location and call API
                }) {
                    Text(location.title + ", " + location.subtitle)
                        .foregroundColor(Color(UIColor.Neutrals.secondary))
                        .font(.system(size: 14, weight: .medium, design: .default))
                }
            }
        }
        .listStyle(.inset)
    }
}

struct NearMeLocationSearchBarView_Previews: PreviewProvider {
    static var previews: some View {
        NearMeLocationSearchBarView()
    }
}

//MARK: - View Model
class NearMeLocationSearchViewModel: ObservableObject {
    
    @Published var region: MKCoordinateRegion = MKCoordinateRegion()
    @Published private var coordinate: CLLocationCoordinate2D?
    @Published var isEditingSearchQuery : Bool = false
    @Published var isManuallyEnteredLocation : Bool = false
    var selectedLocation: MKLocalSearchCompletion?
    
    //UI
    func getViewHeight() -> CGFloat {
        return UIScreen.main.bounds.width * 0.12
    }
    
    //Images
    func getCurrentLocationMarkImage() -> String {
        return "current_location_mark"
    }
    
    func getManualLocationImage() -> String {
        return "manual_location_marker"
    }
    
    func getCurrentLocationListImage() -> String {
        return "filled_location_mark"
    }
    
    func getFilterImage() -> String {
        return "filter"
    }
    
    //Colors
    func getSearchViewActiveColor() -> UIColor {
        return UIColor.Theme.light1
    }
    
    func getSearchViewInactiveColor() -> UIColor {
        return UIColor.Neutrals.background
    }
    
    func getDividerColor() -> UIColor {
        return UIColor.Neutrals.pendingNotification
    }
    
    func fetchLocationDetail(location: MKLocalSearchCompletion) {
        let search = MKLocalSearch(request: MKLocalSearch.Request(completion: location))
        search.start { response, error in
            if error == nil, let coordinate = response?.mapItems.first?.placemark.coordinate {
                self.coordinate = coordinate
                debugPrint("coordinate: \(coordinate)")
            }
        }
    }
}

struct LocationSearchViewData: Identifiable {
    var id = UUID()
    var location: MapMarker
}

//MARK: - Location Service
class LocationSearchService: NSObject, ObservableObject {
    
    @Published var searchQuery : String = ""
    @Published var locationSearchResults : [MKLocalSearchCompletion] = []
    private var searchCompleter = MKLocalSearchCompleter()
    private var cancellables : Set<AnyCancellable> = []
    private var searchPromise : ((Result<[MKLocalSearchCompletion], Error>) -> Void)?
    
    override init() {
        super.init()
        searchCompleter.delegate = self
        
        $searchQuery
            .debounce(for: .seconds(0.5), scheduler: RunLoop.main)
            .removeDuplicates()
            .flatMap({ inputQuery in
                self.fetchSearchResults(searchString: inputQuery)
            })
            .sink(receiveCompletion: { completion in
                //TODO: do something/ handle error
            }, receiveValue: { searchResults in
                self.locationSearchResults = searchResults
            })
            .store(in: &cancellables)
    }
    
    func fetchSearchResults(searchString: String) -> Future<[MKLocalSearchCompletion], Error> {
        Future { promise in
            self.searchCompleter.queryFragment = searchString
            self.searchPromise = promise
        }
    }
}

//MARK: - MKLocalSearchCompleterDelegate
extension LocationSearchService: MKLocalSearchCompleterDelegate {
    func completerDidUpdateResults(_ completer: MKLocalSearchCompleter) {
        searchPromise?(.success(completer.results))
    }
    
    func completer(_ completer: MKLocalSearchCompleter, didFailWithError error: Error) {
        debugPrint(error.localizedDescription)
        self.locationSearchResults.removeAll()
    }
}
