//
//  NearMeEmptyStateView.swift
//  ComponentCreation (iOS)
//
//  Created by Ashish Mankar on 09/05/22.
//

import SwiftUI

struct NearMeEmptyStateView: View {
    var body: some View {
        ZStack {
            VStack{
                ZStack{
                    VStack{
                        Spacer()
                            .frame(height: 32)
                        Image("location_mark")
                        Spacer()
                            .frame(height: 22)
                        Text("We couldn’t find any Matches near you. Try a different location and search!")
                            .multilineTextAlignment(.center)
                            .font(.system(size: CGFloat().getFontSize(14), weight: .medium, design: .default))
                            .padding(.horizontal, 32)
                            .foregroundColor(Color(UIColor.Neutrals.primary))
                        Spacer()
                            .frame(height: 26)
                        BlueGradientButton(title: "Change My Location", horizontalPadding: CGFloat().getAspectSize(44), action: {
                            
                        })
                        Spacer()
                            .frame(height: 30)
                    }
                    .background(Color(UIColor.Other.Gray.deactive))
                    .cornerRadius(10)
                    .padding(.top, 60)
                    .padding(.horizontal, 18)
                }
                .overlay(
                    Image("indicator_arrow")
                        .offset(x: -54,y: -76)
                )
                Spacer()
            }
        }
    }
}

struct NearMeEmptyStateView_Previews: PreviewProvider {
    static var previews: some View {
        NearMeEmptyStateView()
    }
}
