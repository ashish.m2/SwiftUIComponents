//
//  NearMeProfileHeader.swift
//  ComponentCreation (iOS)
//
//  Created by Ashish Mankar on 05/05/22.
//

import SwiftUI

struct NearMeProfileHeader_Previews: PreviewProvider {
    static var previews: some View {
        NearMeProfileHeader()
    }
}

struct NearMeProfileHeader: View {
    var body: some View {
        ZStack{
            ZStack{
                Color(UIColor.PremiumTab.background)
                VStack {
                    Spacer()
                    HStack(spacing: 0){
                        Spacer()
                            .frame(width: 20)
                        ZStack{
                            RadialGradient(colors: [Color(UIColor.SectionGradient.yellowEnd),Color(UIColor.Theme.primary)], center: .bottom, startRadius: 0, endRadius: 18)
                            Image("solid_location_mark")
                                .renderingMode(.template)
                                .resizable()
                                .foregroundColor(Color(UIColor.Theme.primaryContrast))
                                .scaledToFit()
                                .frame(width:10, height: 10)
                                .shadow(color: Color(UIColor.Theme.primaryContrast), radius: 7, x: 0, y: 2)
                        }
                        .clipShape(diamondShape(rect: CGRect(x: 0, y: 0, width: 26, height: 26)))
                        .frame(width: 26, height: 26)
                        .shadow(color: Color(UIColor.Other.Red.vibrantRed).opacity(0.4), radius: 4, x: 0, y: 3)
                        LinearGradient(colors: [Color(UIColor.SectionGradient.yellowEnd),Color(UIColor.Other.Red.vibrantRed)], startPoint: .leading, endPoint: .trailing)
                            .mask(
                                HStack{
                                    Text("Showing Matches Near You From Other Cities")
                                        .font(.system(size: CGFloat().getFontSize(11), weight: .bold, design: .default))
                                        .italic()
                                    Spacer()
                                }
                            )
                            .minimumScaleFactor(0.5)
                            .lineLimit(1)
                            .padding(.horizontal, 20)
                    }
                    Spacer()
                    Spacer()
                        .frame(height: 8)
                }
            }
            .clipShape(headerPath())
            .shadow(color: Color(.black).opacity(0.12), radius: 4, x: 0, y: 1)
            .frame(height: 52)
        }
    }
    
    func diamondShape(rect: CGRect) -> Path {
        var path = Path()
        let cornerRadius:CGFloat = 2
        path.move(to: CGPoint(x: rect.midX - cornerRadius, y: cornerRadius))
        path.addLine(to: CGPoint(x: cornerRadius, y: rect.midY - cornerRadius))
        path.addQuadCurve(to: CGPoint(x: cornerRadius, y: rect.midY + cornerRadius), control: CGPoint(x: rect.minX, y: rect.midY))
        path.addLine(to: CGPoint(x: rect.midX - cornerRadius, y: rect.maxY - cornerRadius))
        path.addQuadCurve(to: CGPoint(x: rect.midX + cornerRadius, y: rect.maxY - cornerRadius), control: CGPoint(x: rect.midX, y: rect.maxY))
        path.addLine(to: CGPoint(x: rect.maxX - cornerRadius, y: rect.midY + cornerRadius))
        path.addQuadCurve(to: CGPoint(x: rect.maxX - cornerRadius, y: rect.midY - cornerRadius), control: CGPoint(x: rect.maxX, y: rect.midY))
        path.addLine(to: CGPoint(x: rect.midX + cornerRadius, y: cornerRadius))
        path.addQuadCurve(to: CGPoint(x: rect.midX - cornerRadius, y: cornerRadius), control: CGPoint(x: rect.midX, y: rect.minY))
        return path
    }
    
    func headerPath() -> Path {
        var path = Path()
        let height:CGFloat = 52
        let angleStart:CGFloat = height - 8
        let mid:CGFloat = 34
        let midDifference:CGFloat = 12
        let midLeft:CGFloat = mid - midDifference
        let midRight:CGFloat = mid + midDifference
        path.move(to: CGPoint(x: 0, y: 0))
        path.addLine(to: CGPoint(x: 0, y: angleStart))
        path.addLine(to: CGPoint(x: midLeft, y: angleStart))
        path.addLine(to: CGPoint(x: mid, y: height))
        path.addLine(to: CGPoint(x: midRight, y: angleStart))
        path.addLine(to: CGPoint(x: UIScreen.main.bounds.width, y: angleStart))
        path.addLine(to: CGPoint(x: UIScreen.main.bounds.width, y: 0))
        path.addLine(to: CGPoint(x: 0, y: 0))
        return path
    }
}
