//
//  NearMeOnBoardingView.swift
//  ComponentCreation (iOS)
//
//  Created by Ashish Mankar on 20/04/22.
//
import Foundation
import SwiftUI

#if DEBUG
struct NearMeOnBoardingView_Previews: PreviewProvider {
    static var previews: some View {
        NearMeOnBoardingView(ctaAction: {}, dismissAction: {})
    }
}
#endif

class NearMeOnBoardingViewModel: ObservableObject {
    @Published var isPermissionGiven: Bool = false
    @Published var dismissed: Bool = false
    
    func dismissView() {
        self.dismissed = true
    }
    
    func getOffsetY() -> CGFloat {
        return self.dismissed ? UIScreen.main.bounds.height : 0
    }
    
    func getTitle() -> String {
        return "Simran, let’s find out if your perfect partner is near you 🤗"
    }
    
    func getSubtitle() -> String {
        return "Allow Location Access to see relevant Matches"
    }
    
    func getCTACaption() -> String {
        return self.isPermissionGiven ? "View Matches in Near Me" : "Allow Location Access"
    }
    
    func getCloseImage() -> String {
        return "close_comparison"
    }

    func getCurveViewHeight() -> CGFloat {
        return UIScreen.main.bounds.width / 1.3
    }
    
    func getSelfImage() -> UIImage {
        return UIImage(named: "demomale") ?? UIImage()
    }
    
    func getOuterCircleImage() -> UIImage {
        return UIImage(named: "demomale") ?? UIImage()
    }
    
    func getInnerCircleImage() -> UIImage {
        return UIImage(named: "demomale") ?? UIImage()
    }
    
    func getOuterCircleDiameter() -> CGFloat {
        return UIScreen.main.bounds.width + 48
    }
    
    func getFirstPicturePosition() -> CGFloat {
        return -(self.getOuterCircleDiameter()/2)
    }
    
    func getSecondPicturePosition() -> CGFloat {
        return -(self.getInnerCircleDiameter()/2)
    }
    
    func getInnerCircleDiameter() -> CGFloat {
        return UIScreen.main.bounds.width - 68
    }
}

struct NearMeOnBoardingView: View {
    @StateObject var viewModel: NearMeOnBoardingViewModel = NearMeOnBoardingViewModel()
    
    var ctaAction: () -> ()
    var dismissAction: () -> ()
    
    var body: some View {
        ZStack{
            VStack{
                ZStack{
                    TertiaryCurveView(viewModel: viewModel)
                        .offset(y: 20)
                        .opacity(0.2)
                    SecondaryCurveView(viewModel: viewModel)
                        .offset(y: 10)
                        .opacity(0.5)
                    CurveAnimatingView(viewModel: viewModel)
                }
                .frame(height: self.viewModel.getCurveViewHeight())
                Spacer()
                Text(self.viewModel.getTitle())
                    .foregroundColor(Color(UIColor.Other.Gray.light))
                    .font(.system(size: CGFloat().getFontSize(20), weight: .bold, design: .default))
                    .multilineTextAlignment(.center)
                    .padding(.horizontal, 20)
                if viewModel.isPermissionGiven {
                    Spacer()
                        .frame(height: 14)
                    Text(self.viewModel.getSubtitle())
                        .foregroundColor(Color(UIColor.Neutrals.tertiary))
                        .font(.system(size: CGFloat().getFontSize(14), weight: .regular, design: .default))
                        .multilineTextAlignment(.center)
                        .padding(.horizontal, 20)
                }
                Spacer()
                    .frame(height: 40)
                Button(action: {
                    debugPrint("Allow Location Access Tapped")
                    withAnimation {
                        self.viewModel.dismissView()
                    }
                }, label: {
                    ZStack{
                        HStack{
                            
                            Spacer()
                            Text(viewModel.getCTACaption())
                                .foregroundColor(Color(UIColor.Theme.primaryContrast))
                                .font(.system(size: CGFloat().getFontSize(16), weight: .bold, design: .default))
                                .padding(.vertical, 16)
                            Spacer()
                        }
                    }
                    .background(LinearGradient(colors: [Color(UIColor.Other.gradientBlue),Color(UIColor.Gradient.bottom)], startPoint: .top, endPoint: .bottom))
                    .cornerRadius(24)
                    .shadow(color: Color(UIColor.Other.gradientBlue).opacity(0.4), radius: 10, x: 0, y: 4)
                })
                    .padding(.horizontal, 50)
                Spacer()
            }
        }
        .offset(y: viewModel.getOffsetY())
    }
}

struct CurveAnimatingView: View {
    var viewModel: NearMeOnBoardingViewModel
    var body: some View {
        ZStack{
            RadialGradientView(viewModel: viewModel)
            VStack{
                HStack{
                    Spacer()
                    Button(action: {
                        debugPrint("Closed")
                        withAnimation {
                            viewModel.dismissView()
                        }
                    }, label: {
                        Image(self.viewModel.getCloseImage())
                            .renderingMode(.template)
                            .resizable()
                            .frame(width: 12, height: 12)
                            .foregroundColor(Color(UIColor.Theme.primaryContrast).opacity(0.6))
                    })
                        .padding(16)
                }
                Spacer()
                Image(uiImage: self.viewModel.getSelfImage())
                    .resizable()
                    .clipShape(Circle())
                    .frame(width: CGFloat().getAspectSize(84), height: CGFloat().getAspectSize(84))
                    .overlay(
                        Circle()
                            .stroke(Color(UIColor.Theme.primaryContrast), style: StrokeStyle(lineWidth:2))
                    )
                Spacer()
                    .frame(height: 26)
            }
            .overlay(
                ConcentricCicleAnimationView(viewModel: viewModel)
            )
        }
        .clipShape(self.path(rect: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: self.viewModel.getCurveViewHeight())))
        .cornerRadius(radius: 20, corners: [.topLeft, .topRight])
        .frame(height: self.viewModel.getCurveViewHeight())
        
    }
    
    func path(rect: CGRect) -> Path {
        var path = Path()
        path.move(to: CGPoint(x: rect.minX, y: rect.minY))
        path.addLine(to: CGPoint(x: rect.minX, y: rect.maxY - 66))
        path.addLine(to: CGPoint(x: rect.midX - 120, y: rect.maxY - 66))
        path.addQuadCurve(to: CGPoint(x: rect.midX - 74, y: rect.maxY - 44), control: CGPoint(x: rect.midX - 120 + 20, y: rect.maxY - 64))
        path.addQuadCurve(to: CGPoint(x: rect.midX + 74, y: rect.maxY - 44), control: CGPoint(x: rect.midX, y: rect.maxY + 10))
        path.addQuadCurve(to: CGPoint(x: rect.midX + 120 , y: rect.maxY - 66),control: CGPoint(x: rect.midX + 120 - 20, y: rect.maxY - 64))
        path.addLine(to: CGPoint(x: rect.maxX, y: rect.maxY - 66))
        path.addLine(to: CGPoint(x: rect.maxX, y: rect.minY))
        path.addLine(to: CGPoint(x: rect.minX, y: rect.minY))
        return path
    }
    
    func circularPath() -> Path {
        var path = Path()
        path.move(to: CGPoint(x: 0, y: 0))
        
        return path
    }

}

struct SecondaryCurveView: View {
    var viewModel: NearMeOnBoardingViewModel
    
    var body: some View {
        ZStack{
            RadialGradientView(viewModel: viewModel)
                .clipShape(self.secondaryPath(rect: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: self.viewModel.getCurveViewHeight())))
        }
        
        
    }
    
    func secondaryPath(rect: CGRect) -> Path {
        var path = Path()
        path.move(to: CGPoint(x: rect.minX, y: rect.minY))
        path.addLine(to: CGPoint(x: rect.minX, y: rect.maxY - 76))
        path.addLine(to: CGPoint(x: rect.midX - 146, y: rect.maxY - 76))
        path.addQuadCurve(to: CGPoint(x: rect.midX - 74, y: rect.maxY - 44), control: CGPoint(x: rect.midX - 120, y: rect.maxY - 78))
        path.addQuadCurve(to: CGPoint(x: rect.midX + 74, y: rect.maxY - 44), control: CGPoint(x: rect.midX, y: rect.maxY + 10))
        path.addQuadCurve(to: CGPoint(x: rect.midX + 120 + 20, y: rect.maxY - 76),control: CGPoint(x: rect.midX + 120 - 24, y: rect.maxY - 64))
        path.addLine(to: CGPoint(x: rect.maxX, y: rect.maxY - 76))
        path.addLine(to: CGPoint(x: rect.maxX, y: rect.minY))
        path.addLine(to: CGPoint(x: rect.minX, y: rect.minY))
        return path
    }
}

struct TertiaryCurveView: View {
    var viewModel: NearMeOnBoardingViewModel
    
    var body: some View {
        ZStack{
            RadialGradientView(viewModel: viewModel)
                .clipShape(self.tertiaryPath(rect: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: self.viewModel.getCurveViewHeight())))
        }
    }
    
    func tertiaryPath(rect: CGRect) -> Path {
        var path = Path()
        path.move(to: CGPoint(x: rect.minX, y: rect.minY))
        path.addLine(to: CGPoint(x: rect.minX, y: rect.maxY - 86))
        path.addLine(to: CGPoint(x: rect.midX - 146, y: rect.maxY - 86))
        path.addQuadCurve(to: CGPoint(x: rect.midX - 74, y: rect.maxY - 44), control: CGPoint(x: rect.midX - 120, y: rect.maxY - 78))
        path.addQuadCurve(to: CGPoint(x: rect.midX + 74, y: rect.maxY - 44), control: CGPoint(x: rect.midX, y: rect.maxY + 10))
        path.addQuadCurve(to: CGPoint(x: rect.midX + 120 + 30, y: rect.maxY - 86),control: CGPoint(x: rect.midX + 120 - 24, y: rect.maxY - 64))
        path.addLine(to: CGPoint(x: rect.maxX, y: rect.maxY - 86))
        path.addLine(to: CGPoint(x: rect.maxX, y: rect.minY))
        path.addLine(to: CGPoint(x: rect.minX, y: rect.minY))
        return path
    }
}

struct RadialGradientView: View {
    var viewModel: NearMeOnBoardingViewModel
    var body: some View {
        ZStack{
            RadialGradient(colors: [Color(UIColor.SectionGradient.yellowEnd),Color(UIColor.Theme.primary)], center: .bottom, startRadius: 0, endRadius: UIScreen.main.bounds.width/2)
        }
        .cornerRadius(radius: 20, corners: [.topLeft, .topRight])
        .frame(height: self.viewModel.getCurveViewHeight())
    }
}

struct ConcentricCicleAnimationView: View {
    var viewModel: NearMeOnBoardingViewModel
    @State private var outerFirstPictureAngle:CGFloat = -90
    @State private var outerSecondPictureAngle:CGFloat = -90
    var body: some View {
        ZStack{
            Group{
                Circle()
                    .stroke(lineWidth: 1)
                    .foregroundColor(Color(UIColor.Theme.primaryContrast))
                    .frame(width: viewModel.getOuterCircleDiameter(), height: viewModel.getOuterCircleDiameter())
                Image(uiImage: self.viewModel.getInnerCircleImage())
                    .resizable()
                    .clipShape(Circle())
                    .frame(width: CGFloat().getAspectSize(42), height: CGFloat().getAspectSize(42))
                    .overlay(Circle().stroke(Color(UIColor.Theme.primaryContrast), style: StrokeStyle(lineWidth: 1)))
                    .shadow(color: .black.opacity(0.3), radius: 17, x: 3, y: -2)
                    .rotationEffect(Angle(degrees: -self.outerFirstPictureAngle))
                    .offset(y: viewModel.getFirstPicturePosition())
                    .rotationEffect(Angle(degrees: self.outerFirstPictureAngle))
                    .animation(Animation.linear(duration: 5).repeatForever(autoreverses: false))
                    .onAppear {
                        self.outerFirstPictureAngle = 90
                    }
            }
            .offset(y: CGFloat().getAspectSize(120))
            
            Group{
                Circle()
                    .stroke(lineWidth: 1)
                    .foregroundColor(Color(UIColor.Theme.primaryContrast))
                    .frame(width: viewModel.getInnerCircleDiameter(), height: viewModel.getInnerCircleDiameter())
                Image(uiImage: self.viewModel.getInnerCircleImage())
                    .resizable()
                    .clipShape(Circle())
                    .frame(width: CGFloat().getAspectSize(42), height: CGFloat().getAspectSize(42))
                    .overlay(Circle().stroke(Color(UIColor.Theme.primaryContrast), style: StrokeStyle(lineWidth: 1)))
                    .shadow(color: .black.opacity(0.3), radius: 17, x: 3, y: -2)
                    .rotationEffect(Angle(degrees: -self.outerSecondPictureAngle))
                    .offset(y: viewModel.getSecondPicturePosition())
                    .rotationEffect(Angle(degrees: self.outerSecondPictureAngle))
                    .animation(Animation.linear(duration: 4).repeatForever(autoreverses: false))
                    .onAppear {
                        self.outerSecondPictureAngle = 135
                    }
            }
            .offset(y: CGFloat().getAspectSize(120))
        }
    }
}
