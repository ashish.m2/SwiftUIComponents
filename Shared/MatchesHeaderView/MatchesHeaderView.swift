//
//  MatchesHeaderView.swift
//  ComponentCreation (iOS)
//
//  Created by Ashish Mankar on 17/02/22.
//

import SwiftUI

struct MatchesHeaderView: View {
    var body: some View {
        ZStack {
            VStack {
                UndoToolTip(undoAction: {})
                Spacer()
                MatchesListHeaderView()
                Spacer()
                    .frame(height: 50)
                MatchesSwipeHeaderView()
                    .padding(.horizontal, 10)
                Spacer()
            }
        }
    }
}

struct MatchesHeaderView_Previews: PreviewProvider {
    static var previews: some View {
        MatchesHeaderView()
    }
}

struct HeaderButtonWithImageAndText: View {
    var image:UIImage
    var title:String
    var body: some View {
        Button(action: {}, label: {
            HStack(spacing: 4){
                Spacer()
                Image(uiImage: image)
                    .resizable()
                    .scaledToFit()
                    .frame(width: 14, height: 14)
                Text(title)
                    .foregroundColor(Color(UIColor.Neutrals.secondary))
                    .font(.system(size: 10, weight: .bold, design: .default))
                    .lineLimit(1)
                Spacer()
            }
            .padding(.vertical, 10)
        })
            .background(Color(UIColor.Neutrals.background))
            .cornerRadius(16)
            .frame(height: 32)
    }
}

struct HeaderButton: View {
    var image:UIImage
    var imageWidthHeight:CGFloat
    var indicatorFlag:Bool = false
    var body: some View {
        Button(action: {}, label: {
            let padding:CGFloat = (32 - imageWidthHeight) / 2
            HStack(spacing: 4){
                Image(uiImage: image)
                    .resizable()
                    .scaledToFit()
                    .frame(width: imageWidthHeight, height: imageWidthHeight)
                if indicatorFlag {
                    Color(UIColor.Accent.primary)
                        .frame(width: 6, height: 6)
                        .cornerRadius(3)
                }
            }
            .padding(padding)
        })
            .background(Color(UIColor.Neutrals.background))
            .cornerRadius(16)
            .frame(width: 32,height: 32)
    }
}

struct MatchesHeaderToolTip: View {
    var title:String = "Tap to switch to List View"
    var offSetX:CGFloat = 78
    var offSetY:CGFloat = 48
    var alignment:Alignment = .center
    @Binding var tooltipFlag:Bool
    var body: some View {
        ZStack(alignment: alignment) {
            Color(.clear)
                .frame(height: 48)
            VStack(alignment: .trailing, spacing: 0){
                HStack{
                    Image("coachUpArrow")
                        .padding(.trailing, 14)
                }
                HStack(spacing: 10){
                    Text(title)
                        .font(.system(size: 14, weight: .medium, design: .default))
                        .foregroundColor(Color(UIColor.Theme.primaryContrast))
                        .lineLimit(1)
                        .minimumScaleFactor(0.01)
                    Button(action: {
                        withAnimation {
                            tooltipFlag.toggle()
                        }
                    }) {
                        Image("close_comparison")
                            .renderingMode(.template)
                            .resizable()
                            .frame(width: 12, height: 12)
                            .foregroundColor(Color(UIColor.Theme.primaryContrast))
                    }
                }
                .padding(14)
                .background(Color(.black))
                .cornerRadius(6)
            }
        }
        .offset(x: offSetX,y: offSetY)
    }
}

struct MatchesSwipeHeaderView: View {
    @State var toolTipFlag:Bool = false
    var body: some View {
        ZStack {
            let spacing = UIScreen.main.bounds.width/30
            HStack(spacing: spacing){
                let rotateImage = UIImage(named: "rotate") ?? UIImage()
                let preferenceImage = UIImage(named: "partner_prefs_stack") ?? UIImage()
                let filter = UIImage(named: "filter_stack") ?? UIImage()
                let showList = UIImage(named: "show_list") ?? UIImage()
                HeaderButtonWithImageAndText(image: rotateImage, title: "Undo Last Swipe")
                HeaderButtonWithImageAndText(image: preferenceImage, title: "Partner Preferences")
                HeaderButton(image: filter, imageWidthHeight: 10,indicatorFlag: true)
                HeaderButton(image: showList, imageWidthHeight: 20)
            }
            .frame(height: 48)
            if toolTipFlag {
                let offSetX:CGFloat = (UIScreen.main.bounds.width / 62 - 10) * -1
                MatchesHeaderToolTip(title: "Tap to switch to List View", offSetX: offSetX, offSetY: 48,alignment: .trailing,tooltipFlag: $toolTipFlag)
            }
        }
        .onAppear {
            withAnimation {
                toolTipFlag.toggle()
            }
        }
    }
}

struct MatchesListHeaderView: View {
    @State var toolTipFlag:Bool = false
    var body: some View {
        ZStack {
            let title = "Matching most of your preferences"
            let spacing = UIScreen.main.bounds.width / 30
            HStack(spacing: spacing) {
                let filter = UIImage(named: "filter_stack") ?? UIImage()
                let showQR = UIImage(named: "show_QR") ?? UIImage()
                Text(title)
                    .foregroundColor(Color(UIColor.Neutrals.secondary))
                    .font(.system(size: 14, weight: .regular, design: .default))
                Button(action: {}) {
                    Text("Edit")
                        .foregroundColor(Color(UIColor.Accent.primary))
                        .font(.system(size: 14, weight: .medium, design: .default))
                }
                Spacer()
                HeaderButton(image: filter, imageWidthHeight: 10,indicatorFlag: true)
                HeaderButton(image: showQR, imageWidthHeight: 14)
            }
            .padding(.horizontal, 12)
            if toolTipFlag {
                let offSetX:CGFloat = UIScreen.main.bounds.width / 62 * -1
                MatchesHeaderToolTip(title: "Tap to switch back to Stack View", offSetX: offSetX, offSetY: 48,alignment: .trailing,tooltipFlag: $toolTipFlag)
            }
        }
        .zIndex(1)
        .onAppear {
            withAnimation {
                toolTipFlag.toggle()
            }
        }
    }
}

struct UndoToolTip: View {
    var undoAction: ()->()
    var body: some View {
        HStack(spacing: 8) {
            Text("You just sent a Connect!")
                .foregroundColor(Color(UIColor.Theme.primaryContrast))
                .font(.system(size: 14, weight: .regular, design: .default))
                .padding(.trailing, 20)
            Button(action: undoAction, label: {
                Text("UNDO")
                    .foregroundColor(Color(UIColor.Theme.primaryContrast))
                    .font(.system(size: 14, weight: .medium, design: .default))
                    .tracking(1.25)
            })
        }
        .padding(.horizontal, 16)
        .padding(.vertical, 14)
        .background(Color(UIColor.Other.blackShade))
        .shadow(color: .black.opacity(0.2), radius: 5, x: 0, y: 5)
    }
}
