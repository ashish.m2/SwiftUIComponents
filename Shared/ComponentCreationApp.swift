//
//  ComponentCreationApp.swift
//  Shared
//
//  Created by Ashish Mankar on 17/11/21.
//

import SwiftUI

@main
struct ComponentCreationApp: App {
    var body: some Scene {
        WindowGroup {
            NearMePrompt(closeAction: {}, updateAction: {})
        }
    }
}
