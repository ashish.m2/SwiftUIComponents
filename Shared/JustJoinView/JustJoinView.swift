//
//  JustJoinView.swift
//  ComponentCreation (iOS)
//
//  Created by Ashish Mankar on 11/02/22.
//

import SwiftUI

struct JustJoinViewModel{
    
    func getGradientColor() -> [Color] {
        return [Color(UIColor.RedGradient.top),Color(UIColor.RedGradient.bottom)]
    }
    
    func getAspectRatioForHeight() ->CGFloat {
        return 356/340
    }
    
    func getAspectRationForWidth() -> CGFloat {
        return 340/356
    }
    
    func getBannerWidth() -> CGFloat {
        return UIScreen.main.bounds.width - 20
    }
    
    func getBannerHeight() -> CGFloat {
        return getBannerWidth() * 356/340
    }
    
    func getOffSetForMidText() -> CGFloat {
        return getBannerHeight() * 0.6
    }
    
    func getProfileImage() -> UIImage {
        return UIImage(named: "demomale") ?? UIImage()
    }
    
    func getJustJoinTag() -> UIImage {
        return UIImage(named: "just_join_tag") ?? UIImage()
    }
    
    func getTitle() -> String {
        let firstName = "FirstName"
        return JustJoinText.title + " " + firstName
    }
}

struct JustJoinText{
    static let title = "Only Premium users can Connect with Just Joined Matches like"
    static let upgradeToConnect = "Upgrade to Connect"
    static let beTheFirstConnect = "👇 Be the first to Connect with him 👇"
    static let notNow = "Not Now"
}

struct JustJoinView: View {
    var viewModel:JustJoinViewModel = JustJoinViewModel()
    @State var showJustJoin:Bool = false
    var body: some View {
        ZStack{
            BackgroundTransparentView()
                .onTapGesture {
                    withAnimation {
                        showJustJoin.toggle()
                    }
                }
            ZStack(alignment:.top){
                JustJoinBackgroundView()
                VStack(spacing: 0) {
                    HStack(spacing: 0){
                        Spacer()
                        Button(action: {
                            withAnimation {
                                showJustJoin.toggle()
                            }
                        }, label: {
                            Image(systemName: "xmark")
                                .foregroundColor(Color(UIColor.Theme.primaryContrast).opacity(0.7))
                                .font(.system(size: 20, weight: .medium, design: .default))
                                .frame(width: 20, height: 20)
                                .padding(EdgeInsets(top: 12, leading: 12, bottom: 0, trailing: 12))
                        })
                    }
                    VStack(spacing: -10){
                        Image(uiImage: viewModel.getProfileImage())
                        Image(uiImage: viewModel.getJustJoinTag())
                    }
                    Color(.clear)
                        .frame(height: 16)
                    Text(JustJoinText.title)
                        .font(.system(size: 18, weight: .bold, design: .default))
                        .multilineTextAlignment(.center)
                        .lineLimit(2)
                        .minimumScaleFactor(0.1)
                        .lineSpacing(8)
                        .foregroundColor(Color(UIColor.Theme.primaryContrast))
                        .padding(EdgeInsets(top: 0, leading: 16, bottom: 0, trailing: 16))
                    Spacer()
                    VStack(spacing: 0){
                        Color(.clear)
                            .frame(height: 32)
                        HStack{
                            Color(.clear)
                                .frame(width: 50)
                            Button(action: {
                                withAnimation {
                                    showJustJoin.toggle()
                                }
                            }, label: {
                                ZStack {
                                    LinearGradient(gradient: Gradient(colors: [Color(UIColor.RedGradient.top),Color(UIColor.RedGradient.bottom)]), startPoint: /*@START_MENU_TOKEN@*/.leading/*@END_MENU_TOKEN@*/, endPoint: /*@START_MENU_TOKEN@*/.trailing/*@END_MENU_TOKEN@*/)
                                        .cornerRadius(22)
                                        .frame(height: 44)
                                    Text(JustJoinText.upgradeToConnect)
                                        .font(.system(size: 16, weight: .bold, design: .default))
                                        .foregroundColor(Color(UIColor.Theme.primaryContrast))
                                }
                            })
                            .shadow(color: Color(UIColor.RedGradient.top), radius: 4, x: 0, y: 2)
                            Color(.clear)
                                .frame(width: 50)
                        }
                        .frame(height: 44)
                        Color(.clear)
                            .frame(height: 16)
                        HStack{
                            Button(action: {
                                withAnimation {
                                    showJustJoin.toggle()
                                }
                            }, label: {
                                Text(JustJoinText.notNow)
                                    .font(.system(size: 16, weight: .regular, design: .default))
                                    .foregroundColor(Color(UIColor.Neutrals.secondary))
                            })
                        }
                        Color(.clear)
                            .frame(height: 24)
                    }
                }
                Text(JustJoinText.beTheFirstConnect)
                    .font(.system(size: 14, weight: .medium, design: .default))
                    .foregroundColor(Color(UIColor.Neutrals.tertiary))
                    .italic()
                    .offset(y: viewModel.getOffSetForMidText())
            }
            .frame(width: viewModel.getBannerWidth(), height: viewModel.getBannerHeight())
            .offset(y: showJustJoin ? 0 : UIScreen.main.bounds.height)
        }
        .edgesIgnoringSafeArea(.all)
        .onAppear(perform: {
            withAnimation {
                showJustJoin.toggle()
            }
        })
    }
}

struct JustJoinView_Previews: PreviewProvider {
    static var previews: some View {
        JustJoinView()
    }
}

struct JustJoinBackgroundView: View {
    let viewModel = JustJoinViewModel()
    var body: some View {
        VStack(spacing: 0){
            Image("justjoinbackground")
                .resizable()
                .aspectRatio(contentMode: .fit)
                .frame(width: viewModel.getBannerWidth())
            Spacer()
        }
        .background(Color(UIColor.Theme.primaryContrast))
        .cornerRadius(12)
    }
}

struct BackgroundTransparentView: View {
    var body: some View {
        Color(UIColor.Other.blackShade)
            .opacity(0.8)
    }
}
