//
//  File.swift
//  ComponentCreation (iOS)
//
//  Created by Ashish Mankar on 17/11/21.
//

import SwiftUI

extension UIColor {
    
    struct Theme {
        static let primary = UIColor(hex: 0xFF5A60)
        static let primaryContrast = UIColor(hex: 0xFFFFFF)
        
        static let medium = UIColor(hex: 0xFF8B8F)
        static let light1 = UIColor(hex: 0xFEC6C8)
        static let light2 = UIColor(hex: 0xFDEDEE)
        static let error = UIColor(hex: 0xE53A41)
        static let appStatus = UIColor(hex: 0xF14D53)
        static let vip = UIColor(hex: 0xAD2241)
        static let secondary = UIColor(hex: 0xd93b3b)
        static let shaadiLiveGradient = UIColor(hex: 0xF93961)
    }
    
    struct Accent {
        static let primary = UIColor(hex: 0x00BCD5)
        static let medium = UIColor(hex: 0x83E1ED)
        static let icon = UIColor(hex: 0xDBF7FB)
        static let primaryButtonHover = UIColor(hex: 0x0194A8)
        static let secondaryButtonHover = UIColor(hex: 0x1BA3B6)
    }
    
    struct Neutrals {
        static let primary = UIColor(hex: 0x51505D)
        static let secondary = UIColor(hex: 0x72727D)
        static let tertiary = UIColor(hex: 0x95959D)
        static let hint = UIColor(hex: 0xB1B3B9)
        static let deactive = UIColor(hex: 0xCDCED1)
        static let divider = UIColor(hex: 0xDFE0E3)
        static let background = UIColor(hex: 0xF1F1F2)
        static let lightBackground = UIColor(hex: 0xF1F1F1)
        static let pendingNotification = UIColor(hex: 0xF4F4F7)
    }
    
    struct Other {
        static let ochreYellow = UIColor(hex: 0xFFB400)
        static let pistachioGreen = UIColor(hex: 0x89C965)
        static let turquoise = UIColor(hex: 0x01D1C1)
        static let lavender = UIColor(hex: 0x9375CB)
        static let columbiaBlue = UIColor(hex: 0x85C1FF)
        static let warmPeach = UIColor(hex: 0xFAA078)
        static let notificationRed = UIColor(hex: 0xFF0000)
        static let darkGreen = UIColor(hex: 0x0ED279)
        static let dullOrange = UIColor(hex: 0xD1A536)
        static let blackShade = UIColor(hex: 0x212126)
        static let blueShade = UIColor(hex: 0x542785)
        static let purpleShade = UIColor(hex: 0xAE89F5)
        static let goldShade = UIColor(hex: 0xE4B760)
        static let successGreen = UIColor(hex: 0x05b766)
        static let feedbackHighlight = UIColor(hex: 0x212126)
        static let messageTitle = UIColor(hex: 0x399ce0)
        static let actionStatus = UIColor(hex: 0x84848d)
        static let actionStatusPale = UIColor(hex: 0x92929d)
        static let secondaryBlue = UIColor(hex: 0x40a9f0)
        static let postiveActionButtonShadow = UIColor(hex: 0x20bccf)
        static let error = UIColor(hex: 0xd83137)
        static let textFieldBorderColor = UIColor(hex: 0xdadbdd)
        static let tealBlue = UIColor(hex:0x0195a8)
        static let lightOrange = UIColor(hex:0xe9ce82)
        static let darkOrange = UIColor(hex:0xcfa230)
        static let actionDocBackground = UIColor(hex: 0x555657)
        static let searchKeywordBackground = UIColor(hex: 0xf7f7f8)
        static let selectedSegment = UIColor(hex: 0xCE4D4D)
        static let mangoOrange = UIColor(hex: 0xffc062)
        static let purple = UIColor(hex: 0xb7a2dc)
        static let skyBlue = UIColor(hex: 0x00D2F5)
        static let sectionHeaderShadow = UIColor(hex: 0xbcc2ca)
        static let darkGreenPremium = UIColor(hex:0x09bf6c)
        static let mildGrey = UIColor(hex: 0xD8D8D8)
        static let dustGrey = UIColor(hex: 0x969696)
        static let primaryOrange = UIColor(hex:0xf85157)
        static let secondaryActionButtonShadow = UIColor(hex: 0x505050)
        static let chatBubbleBlue = UIColor(red: 0.91, green: 0.96, blue: 1.00, alpha: 1)
        static let declineTextColor = UIColor(red: 0.52, green: 0.52, blue: 0.55, alpha: 1)
        static let acceptTextColor = UIColor(red: 0.00, green: 0.74, blue: 0.84, alpha: 1)
        static let shimmerColor = UIColor(hex: 0xF4F2F5)
        static let lightBlue = UIColor(hex: 0x06BDD5)
        static let gradientBlue = UIColor(hex: 0x60CED4)
        static let gold         = UIColor(hex: 0xDDBB5C)
        
        struct Gray{
            static let light = UIColor(hex: 0x41404D)
            static let lighter = UIColor(hex: 0xe6e6e6)
            static let deactive = UIColor(hex: 0xf9f9fb)
            static let textColor = UIColor(hex: 0xBFC1C5)
        }

        struct Red {
            static let vibrantRed = UIColor(hex: 0xFF585C)
            static let notificatoinRed = UIColor(hex: 0xF72125)
        }
    }
    
    struct Gradient {
        static let top          = UIColor(hex: 0x4AC9D4)
        static let bottom       = UIColor(hex: 0x00BCD5)
        static let highlighted  = UIColor(hex: 0x0DD2F5)
        static let orangeTop    = UIColor(hex: 0xf69b33)
        static let orangeBottom = UIColor(hex: 0xfeae52)
        static let purpleTop    = UIColor(hex: 0xa88fd5)
        static let purpleBottom = UIColor(hex: 0x9477cb)
    }
    
    struct GreenGradient {
        static let start        = UIColor(hex: 0x76BB58)
        static let end          = UIColor(hex: 0x21C9DE)
    }
    
    struct RedGradient {
        static let top      = UIColor(hex: 0xFF5A60)
        static let bottom   = UIColor(hex: 0xF93961)
    }
    
    struct SectionGradient{
        static let greenStart = UIColor(hex: 0x8ec966)
        static let greenEnd = UIColor(hex: 0xa1db7a)
        static let mangoOrangeStart = UIColor(hex: 0xf69b33)
        static let mangoOrangeEnd = UIColor(hex: 0xfeae52)
        static let purpleStart = UIColor(hex: 0xaf7ac0)
        static let purpleEnd = UIColor(hex: 0xdb83a5)
        static let yellowStart = UIColor(hex: 0xf5b400)
        static let yellowEnd = UIColor(hex: 0xffcb2e)
        static let blueStart = UIColor(hex: 0x9477cb)
        static let blueEnd = UIColor(hex: 0xa88fd5)
    }
    
    struct CapsuleColors {
        static let futureGradientLight = UIColor(hex: 0xf5b400)//UIColorFromRGB(0xf5b400)
        static let futureGradientDark = UIColor(hex: 0xffcb2e)//UIColorFromRGB(0xffcb2e)
    }
    
    struct PaymentPageColors{
        static let proudctBackgroundColor = UIColor.black.withAlphaComponent(0.16)
        static let proudctBackgroundColorDeselected = UIColor.black.withAlphaComponent(0.016)
        static let cellBackgroundColor = UIColor.black.withAlphaComponent(0.3)
        static let faqSelectedBackground =  UIColor(hex:0xfafafa )
        static let backgroundColor  = UIColor(hex:0xF6F6F4)
        static let continueGradient1 = UIColor(red: 0.38, green: 0.81, blue: 0.83, alpha: 1)
        static let continueGradient2 = UIColor(red: 0.00, green: 0.74, blue: 0.84, alpha: 1)
    }
    
    
    struct PremiumMessaging {
        static let contactNotDeducted = UIColor(hex: 0xe53a41)
        static let connectSent = UIColor(hex: 0x05b766)
        static let contactPrimary = UIColor(hex: 0x41404d)
        static let contactSecondary = UIColor(hex: 0x84848d)
        static let noWhatsApp = UIColor(hex: 0xe74b52)
    }
    
    struct CSATSmileyColor {
        static let hate     = UIColor(hex: 0xFF0000)
        static let dislike  = UIColor(hex: 0xFF6600)
        static let ok       = UIColor(hex: 0xFF9233)
        static let liked    = UIColor(hex: 0xFFB400)
        static let loved    = UIColor(hex: 0x00CC66)
    }
    
    struct GradientButton {
        static let start          = UIColor(hex: 0x1fbbce)
        static let end            = UIColor(hex: 0x76bb58)
    }
    
    struct PremiumTab {
        static let background = UIColor(hex: 0xF9F9F9)
        static let darkThemeColor = UIColor(hex: 0xFFA3861)
    }
    
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    convenience init(hex:Int) {
        self.init(red:(hex >> 16) & 0xff, green:(hex >> 8) & 0xff, blue:hex & 0xff)
    }
}

struct CornerRadiusStyle: ViewModifier {
    var radius: CGFloat
    var corners: UIRectCorner

    struct CornerRadiusShape: Shape {

        var radius = CGFloat.infinity
        var corners = UIRectCorner.allCorners

        func path(in rect: CGRect) -> Path {
            let path = UIBezierPath(roundedRect: rect, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
            return Path(path.cgPath)
        }
    }

    func body(content: Content) -> some View {
        content
            .clipShape(CornerRadiusShape(radius: radius, corners: corners))
    }
}

struct AccessibilityIDs: ViewModifier {
    var identifier:String
    var value:String
    var index:Int?
    func body(content: Content) -> some View {
        let postFix = index != nil ? "_\(index ?? 0)" : ""
        let identifierText = "\(identifier)\(postFix)"
        return content
            .accessibility(value: Text(value))
            .accessibility(label: Text(identifier))
            .accessibility(identifier: identifierText)
    }
}

struct NavigationBarModifier: ViewModifier {

    var backgroundColor: UIColor?
    var titleColor: UIColor?

    init(backgroundColor: UIColor?, titleColor: UIColor?) {
        self.backgroundColor = backgroundColor
        let coloredAppearance = UINavigationBarAppearance()
        coloredAppearance.configureWithTransparentBackground()
        coloredAppearance.backgroundColor = backgroundColor
        coloredAppearance.titleTextAttributes = [.foregroundColor: titleColor ?? .white]
        coloredAppearance.largeTitleTextAttributes = [.foregroundColor: titleColor ?? .white]

        UINavigationBar.appearance().standardAppearance = coloredAppearance
        UINavigationBar.appearance().compactAppearance = coloredAppearance
        UINavigationBar.appearance().scrollEdgeAppearance = coloredAppearance
    }

    func body(content: Content) -> some View {
        ZStack{
            content
            VStack {
                GeometryReader { geometry in
                    Color(self.backgroundColor ?? .clear)
                        .frame(height: geometry.safeAreaInsets.top)
                        .edgesIgnoringSafeArea(.top)
                    Spacer()
                }
            }
        }
    }
}

extension View {
    func cornerRadius(radius: CGFloat, corners: UIRectCorner) -> some View {
        ModifiedContent(content: self, modifier: CornerRadiusStyle(radius: radius, corners: corners))
    }
    
    func navigationBarColor(backgroundColor: UIColor?, titleColor: UIColor?) -> some View {
        self.modifier(NavigationBarModifier(backgroundColor: backgroundColor, titleColor: titleColor))
    }
}

struct MyFrame : Equatable {
    let id : String
    let frame : CGRect

    static func == (lhs: MyFrame, rhs: MyFrame) -> Bool {
        lhs.id == rhs.id && lhs.frame == rhs.frame
    }
}

struct MyKey : PreferenceKey {
    typealias Value = [MyFrame] // The list of view frame changes in a View tree.

    static var defaultValue: [MyFrame] = []

    /// When traversing the view tree, Swift UI will use this function to collect all view frame changes.
    static func reduce(value: inout [MyFrame], nextValue: () -> [MyFrame]) {
        value.append(contentsOf: nextValue())
    }
}

extension CGFloat {
    
    private func getFloatConstant(fontSize: CGFloat) -> CGFloat {
        return 375/fontSize
    }

    private func getFloatConstant(size: CGFloat) -> CGFloat {
        return 375/size
    }

    func getFontSize(_ fontSize: CGFloat) -> CGFloat {
        return CGFloat(Int(UIScreen.main.bounds.width/self.getFloatConstant(fontSize: fontSize)))
    }

    func getAspectSize(_ size: CGFloat) -> CGFloat {
        return CGFloat(Int(UIScreen.main.bounds.width/self.getFloatConstant(fontSize: size)))
    }
}
