//
//  ContentView.swift
//  Shared
//
//  Created by Ashish Mankar on 17/11/21.
//

import SwiftUI

struct NavigationConfigurator: UIViewControllerRepresentable {
    var configure: (UINavigationController) -> Void = { _ in }

    func makeUIViewController(context: UIViewControllerRepresentableContext<NavigationConfigurator>) -> UIViewController {
        UIViewController()
    }
    func updateUIViewController(_ uiViewController: UIViewController, context: UIViewControllerRepresentableContext<NavigationConfigurator>) {
        if let nc = uiViewController.navigationController {
            self.configure(nc)
        }
    }

}

struct ExpiringWidgetView: View {
    var title:String = "13 Requests Expiring Soon"
    var subtitle:String = "3 Requests expire today!"
    var buttonTitle:String = "Respond Now"
    var body: some View {
        ZStack{
            HStack(spacing:4){
                ProfilePictures(picturesName:[UIImage(),UIImage()])
                WidgetTitle(title: title,subtitle: subtitle)
                Spacer()
                WidgetCallToAction(title: buttonTitle)
            }
        }
        .frame(height: 52)
    }
}

struct ContentView: View {
    @State var showLearnMoreView:Bool = false
    var body: some View {
        ZStack(alignment:.bottom){
            TransparentBackgroundView()
            BottomSheetView {
                withAnimation {
                    showLearnMoreView.toggle()
                }
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        Group{
            ContentView()
            ConfirmationView(closeAction: {}, confirmAction: {})
            ExpiringWidgetView()
            LearnMoreView(backgroundHidden: false, closeAction: {})
        }
    }
}
