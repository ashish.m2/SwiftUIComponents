//
//  PhotoHiddenCase.swift
//  ComponentCreation (iOS)
//
//  Created by Ashish Mankar on 16/02/22.
//

import SwiftUI

struct PhotoHiddenCase: View {
    var viewModel:MatchesCardViewModel = MatchesCardViewModel()
    var body: some View {
        ZStack{
            Color(.white)
                //.opacity(0.2)
            VStack(spacing: 0){
                Image(uiImage: viewModel.getPhotoLockImage())
                    .resizable()
                    .frame(width: 34, height: 36)
                    .shadow(color: .black.opacity(0.5), radius: 5, x: 0, y: 0)
                Text(viewModel.getPhotoHiddenTitle())
                    .foregroundColor(Color(UIColor.Theme.primaryContrast))
                    .font(.system(size: 14, weight: .medium, design: .default))
                    .italic()
                    .padding(EdgeInsets(top: 20, leading: 48, bottom: 16, trailing: 48))
                    .lineLimit(2)
                    .lineSpacing(4)
                    .multilineTextAlignment(.center)
                    .minimumScaleFactor(0.5)
                    .shadow(color: .black.opacity(0.5), radius: 5, x: 0, y: 0)
                if viewModel.isUpgradeToViewPhoto() {
                    Button(action: {}, label: {
                        ZStack {
                            Text("Upgrade to View Photo")
                                .foregroundColor(Color(UIColor.Theme.primaryContrast))
                                .padding(EdgeInsets(top: 8, leading: 10, bottom: 8, trailing: 10))
                                .font(.system(size: 14, weight: .bold, design: .default))
                                .padding()
                        }
                        .background(Color(UIColor.Accent.primary))
                        .frame(height: 40)
                        .cornerRadius(20)

                    })
                    .shadow(color: Color(UIColor.Accent.primary).opacity(0.4), radius: 4, x: 0, y: 4)
                }
            }
            //.offset(y: self.viewModel.getPhotoCaseOffSet())
        }
        .frame(width: viewModel.getCardWidth(), height: viewModel.getCardHeight())
    }
}

struct PhotoHiddenCase_Previews: PreviewProvider {
    static var previews: some View {
        PhotoHiddenCase()
    }
}
