//
//  NoPhotoCases.swift
//  ComponentCreation (iOS)
//
//  Created by Ashish Mankar on 16/02/22.
//

import SwiftUI

struct NoPhotoCases: View {
    var viewModel:MatchesCardViewModel = MatchesCardViewModel()
    var body: some View {
        ZStack(alignment:.center){
            Color(UIColor.Neutrals.lightBackground)
            VStack(spacing: 0){
                Image(uiImage: viewModel.getPhotoCaseImage())
                    .resizable()
                    .frame(width: self.viewModel.getPhotoCaseWidth(), height: self.viewModel.getPhotoCaseWidth())
                    .shadow(color: .black.opacity(0.2), radius: 5, x: 0, y: 0)
                Text(viewModel.getPhotoCaseTitle())
                    .foregroundColor(Color(UIColor.Neutrals.primary))
                    .font(.system(size: 14, weight: .medium, design: .default))
                    .italic()
                    .padding(EdgeInsets(top: 4, leading: 0, bottom: 12, trailing: 0))
                    .shadow(color: .black.opacity(0.2), radius: 5, x: 0, y: 0)
                if viewModel.isRequestPhoto() {
                    Button(action: {}, label: {
                        ZStack {
                            Text("Request a Photo")
                                .foregroundColor(Color(UIColor.Theme.primaryContrast))
                                .padding(EdgeInsets(top: 8, leading: 10, bottom: 8, trailing: 10))
                                .font(.system(size: 14, weight: .bold, design: .default))
                                .padding()
                        }
                        .background(Color(UIColor.Accent.primary))
                        .frame(height: 40)
                        .cornerRadius(20)

                    })
                    .shadow(color: Color(UIColor.Accent.primary).opacity(0.4), radius: 4, x: 0, y: 4)
                }
            }
        }
        .frame(width: viewModel.getCardWidth(), height: viewModel.getCardHeight())
    }
}

struct NoPhotoCases_Previews: PreviewProvider {
    static var previews: some View {
        NoPhotoCases()
    }
}
