//
//  AutoMoveToExpiredView.swift
//  ComponentCreation (iOS)
//
//  Created by Ashish Mankar on 09/03/22.
//

import SwiftUI

struct AutoMoveToExpiredView: View {
    var numberOfDays:Int = 14
    var body: some View {
        ZStack {
            VStack{
                Image("green_tick")
                Spacer()
                    .frame(height: 40)
                Group{
                    Text("To ensure our users get a closure, all your received Requests ")
                        .font(.system(size: 16, weight: .regular, design: .default))
                        .foregroundColor(Color(UIColor.Neutrals.primary))
                    +
                    Text("expire in \(numberOfDays) days ")
                        .font(.system(size: 16, weight: .medium, design: .default))
                        .foregroundColor(Color(UIColor.Neutrals.primary))
                    +
                    Text("and are moved to ")
                        .font(.system(size: 16, weight: .regular, design: .default))
                        .foregroundColor(Color(UIColor.Neutrals.primary))
                    +
                    Text("Expired folder")
                        .font(.system(size: 16, weight: .medium, design: .default))
                        .foregroundColor(Color(UIColor.Neutrals.primary))
                    +
                    Text(" after expiry.")
                        .font(.system(size: 16, weight: .regular, design: .default))
                        .foregroundColor(Color(UIColor.Neutrals.primary))
                }
                .lineSpacing(4)
                .padding(.horizontal, 38)
                .multilineTextAlignment(.center)
                Spacer()
                    .frame(height: 24)
                HStack(spacing: 8){
                    ProgressView()
                        .progressViewStyle(CircularProgressViewStyle())
                    Text("Moving to Expired Requests")
                        .foregroundColor(Color(UIColor.Neutrals.primary))
                        .font(.system(size: 16, weight: .regular, design: .default))
                        .italic()
                }
            }
            
        }
    }
}

struct AutoMoveToExpiredView_Previews: PreviewProvider {
    static var previews: some View {
        AutoMoveToExpiredView()
    }
}
