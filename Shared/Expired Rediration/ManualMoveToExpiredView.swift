//
//  ManualMoveToExpiredView.swift
//  ComponentCreation (iOS)
//
//  Created by Ashish Mankar on 09/03/22.
//

import SwiftUI

struct ManualMoveToExpiredView: View {
    var numberOfDays:Int = 14
    var body: some View {
        ZStack{
            VStack{
                Image("QR_no_request")
                Text("No pending Requests")
                    .foregroundColor(Color(UIColor.Neutrals.primary))
                    .font(.system(size: 16, weight: .bold, design: .default))
                Spacer()
                    .frame(height: 24)
                Text("To ensure our users get a closure, all your received Requests expire in \(numberOfDays) days and are moved to Expired folder after expiry.")
                    .foregroundColor(Color(UIColor.Neutrals.secondary))
                    .font(.system(size: 14, weight: .regular, design: .default))
                    .padding(.horizontal, 38)
                    .multilineTextAlignment(.center)
                    .lineSpacing(4)
                Spacer()
                    .frame(height: 18)
                Button(action: {}, label: {
                    ZStack{
                        Text("View Expired Requests")
                            .font(.system(size: 16, weight: .bold, design: .default))
                            .foregroundColor(Color(UIColor.Theme.primaryContrast))
                            .padding(.horizontal,30)
                            .padding(.vertical, 10)
                            .background(LinearGradient(colors: [Color(UIColor.Other.gradientBlue),Color(UIColor.Accent.primary)], startPoint: .top, endPoint: .bottom))
                            .cornerRadius(20)
                    }
                    .shadow(color: .black.opacity(0.2), radius: 8, x: 0, y: 4)
                })
            }
        }
    }
}

struct ManualMoveToExpiredView_Previews: PreviewProvider {
    static var previews: some View {
        ManualMoveToExpiredView()
    }
}
