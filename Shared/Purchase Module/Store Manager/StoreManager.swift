//
//  StoreManager.swift
//  ComponentCreation (iOS)
//
//  Created by Ashish Mankar on 04/03/22.
//

import UIKit
import StoreKit

class StoreManager: NSObject, ObservableObject, SKProductsRequestDelegate, SKPaymentTransactionObserver {
    
    @Published var myProducts = [SKProduct]()
    var request:SKProductsRequest!
    @Published var transactionState: SKPaymentTransactionState?
    
    func getProducts()   {
        print("Start requesting products ...")
        request = SKProductsRequest(productIdentifiers: Set(Product.allCases.compactMap{ $0.rawValue }))
        request.delegate = self
        request.start()
        SKPaymentQueue.default().add(self)
        SKPaymentQueue.default().add(self)
    }
    
    enum Product: String, CaseIterable {
        case gold = "com.temporary.gold"
        case goldplus = "com.temporary.goldplus"
        case platinium = "com.temporary.platinium"
        case renewgold = "com.temporary.renewgold"
        case renewgoldplus = "com.temporary.renewgoldplus"
        case renewplatinium = "com.temporary.renewplatinium"
        case goldauto = "GOLD.AUTO.IPHONE"
    }
    
    func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
        debugPrint("Store Manager Product Request")
        
        if !response.products.isEmpty {
            DispatchQueue.main.async {
                self.myProducts = response.products
                for product in response.products {
                    debugPrint("Product: \(product)")
                }
            }
        }
        
        for invalidIdentifier in response.invalidProductIdentifiers {
            print("Invalid identifiers found: \(invalidIdentifier)")
        }
    }
    
    func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        for transaction in transactions {
            switch transaction.transactionState {
            case .purchasing:
                self.transactionState = .purchasing
                debugPrint("Product Purchasing")
            case .purchased:
                self.transactionState = .purchased
                debugPrint("Product Purchased")
                queue.finishTransaction(transaction)
            case .restored:
                queue.finishTransaction(transaction)
                self.transactionState = .restored
                debugPrint("Product Resotred")
                queue.finishTransaction(transaction)
            case .failed,.deferred:
                self.transactionState = .failed
                debugPrint("Product Failed/Deferred")
                queue.finishTransaction(transaction)
            default:
                queue.finishTransaction(transaction)
            }
        }
    }
    
    func purchaseProduct(product: SKProduct){
        if SKPaymentQueue.canMakePayments() {
            let payment = SKPayment(product: product)
            SKPaymentQueue.default().add(payment)
        } else {
            debugPrint("Can't make payment.")
        }
    }
    
    func request(_ request: SKRequest, didFailWithError error: Error) {
        print("Request did fail: \(error)")
    }
    
    func refreshPurchasedProducts() {
        
    }
}
