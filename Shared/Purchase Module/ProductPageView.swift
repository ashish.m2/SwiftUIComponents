//
//  ProductPageView.swift
//  ComponentCreation (iOS)
//
//  Created by Ashish Mankar on 04/03/22.
//

import SwiftUI

struct ProductPageView: View {
    @StateObject var storeManager:StoreManager = StoreManager()
    var body: some View {
        ZStack{
            GeometryReader { geometry in
                List(storeManager.myProducts, id: \.self){ product in
                    VStack {
                        Text("Product Name: \(product.localizedTitle)")
                            .transformAnchorPreference(key: MyKey.self, value: .bounds) {
                                $0.append(MyFrame(id: product.productIdentifier, frame: geometry[$1]))
                            }
                            .onPreferenceChange(MyKey.self) { value in
                                print(value)
                            }
                        Button(action: {
                            storeManager.purchaseProduct(product: product)
                            debugPrint("====== Product : \(product.localizedTitle)=======")
                        }, label: {
                            Text("Buy it for $ \(product.price)")
                        })
                    }
                }
            }
            
            
        }
        .onAppear {
            storeManager.getProducts()
        }
    }
}

struct ProductPageView_Previews: PreviewProvider {
    static var previews: some View {
        ProductPageView()
    }
}
