//
//  MatchesCardBottomView.swift
//  ComponentCreation (iOS)
//
//  Created by Ashish Mankar on 27/01/22.
//

import SwiftUI

struct MatchesCardBottomViewModel {
    func getCount() -> String {
        return "+5"
    }
    
    func getPictures() -> [UIImage] {
        return [UIImage(named: "male_member") ?? UIImage(), UIImage(named: "trust_badge_gold") ?? UIImage(), UIImage(named: "trust_badge_green") ?? UIImage()]
    }
    
    func getExpandedHeaderText() -> String {
        return "20 Matches Waiting For You To Contact"
    }
}

struct MatchesCardBottomView: View {
    var viewModel:MatchesCardBottomViewModel = MatchesCardBottomViewModel()
    @State var tapped:Bool = false
    @State var animationCompletion:Bool = false
    var body: some View {
        ZStack{
            Color(.gray)
            VStack(spacing: 0){
                if tapped == false {
                    Color(UIColor.Theme.primaryContrast)
                        .opacity(0)
                }
                if animationCompletion == false {
                    ContactedMatchesView(viewModel: viewModel)
                        .onTapGesture {
                            withAnimation(.easeIn) {
                                tapped.toggle()
                            }
                            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                                withAnimation(.easeIn) {
                                    self.animationCompletion.toggle()
                                }
                            }
                            
                        }
                        .opacity(animationCompletion ? 0 : 1)
                }
                if animationCompletion {
                    ContactedMatchesExpandHeader(viewModel: viewModel) {
                        withAnimation(.easeIn) {
                            tapped.toggle()
                        }
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                            withAnimation(.easeIn) {
                                self.animationCompletion.toggle()
                            }
                        }
                    }
                    .opacity(animationCompletion ? 1 : 0)
                    .zIndex(1)
                }
                if tapped {
                    ZStack(alignment: .top){
                        Color(UIColor.Theme.primaryContrast)
                            .frame(height: 0.5)
                            .shadow(color: .black, radius: 6, x: 0, y: 2)
                            .zIndex(/*@START_MENU_TOKEN@*/1.0/*@END_MENU_TOKEN@*/)
                        Color(UIColor.Theme.primaryContrast)
                    }
                    .opacity(tapped ? 1 : 0)
                }
            }
        }
    }
}

struct MatchesCardBottomView_Previews: PreviewProvider {
    static var previews: some View {
        MatchesCardBottomView()
    }
}

struct ContactedMatches: View {
    var viewModel:MatchesCardBottomViewModel = MatchesCardBottomViewModel()
    var body: some View {
        HStack(spacing: 0){
            HStack(spacing: -36){
                let pictures:[UIImage] = viewModel.getPictures()
                //var index:Double = 1
                ForEach(Array(pictures.enumerated()), id: \.element) { index, element in
                    Image(uiImage: element)
                        .resizable()
                        .scaledToFit()
                        .clipShape(Circle())
                        .overlay(Circle().stroke(Color.gray,lineWidth: 1))
                        //.zIndex(1 - (Double(index) * 0.2))
                        .frame(width: 24)
                }
            }
            .frame(width: 48)
            
            Text(viewModel.getCount())
                .foregroundColor(Color(UIColor.Theme.primary))
                .font(.system(size: 12, weight: .bold, design: .default))
                .italic()
                .padding(4)
        }
        .padding(EdgeInsets(top: 4, leading: 8, bottom: 4, trailing: 8))
        .frame(height: 32)
        .background(Color(UIColor.Theme.primaryContrast))
        .cornerRadius(16)
        .shadow(radius: 2)
    }
    
    func reduceZIndex(index: Double)-> Double{
        return index - 0.1
    }
}

struct ContactedMatchesView: View {
    var viewModel: MatchesCardBottomViewModel
    var body: some View {
        VStack(alignment: .center,spacing: -16){
            ContactedMatches(viewModel:viewModel)
                .zIndex(1.0)
            ZStack{
                LinearGradient(gradient: Gradient(colors: [Color(UIColor.RedGradient.top), Color(UIColor.RedGradient.bottom)]), startPoint: .top, endPoint: .bottom)
                    .cornerRadius(radius: 12, corners: [.topLeft, .topRight])
                Text("Swipe up and Contact your Connects")
                    .foregroundColor(Color(UIColor.Theme.primaryContrast))
                    .font(.system(size: 12, weight: .bold, design: .default))
                    .italic()
                    .offset(y: 6)
                    .zIndex(/*@START_MENU_TOKEN@*/1.0/*@END_MENU_TOKEN@*/)
            }
        }
        .frame(height: 64)
        .zIndex(1)
    }
}

struct ContactedMatchesExpandHeader: View {
    var viewModel: MatchesCardBottomViewModel
    var closeAction: ()->()
    var body: some View {
        ZStack(alignment: .bottom){
            Color(.clear)
            VStack(spacing: 0){
                Color(.clear)
                    .frame(height: 80)
                ZStack(alignment:.top){
                    VStack(spacing: 0){
                        Color(UIColor.Theme.primary)
                            .frame(height: 4)
                        HStack(spacing: 0){
                            CornerCurve(color: UIColor.Theme.primary)
                            Spacer()
                            CornerCurve(color: UIColor.Theme.primary)
                                .rotationEffect(.degrees(90))
                                .offset(x: -2, y: -2)
                        }
                    }
                    VStack(spacing: 0){
                        Spacer()
                        HStack(spacing: 0){
                            Text(viewModel.getExpandedHeaderText())
                                .foregroundColor(Color(UIColor.Neutrals.primary))
                                .font(.system(size: 12, weight: .semibold, design: .default))
                        }
                        Spacer()
                    }
                    VStack(spacing: 0){
                        Spacer()
                        HStack{
                            Spacer()
                            Image("closeOnBoarding")
                                .resizable()
                                .frame(width: 12, height: 12)
                                .padding(EdgeInsets(top: 4, leading: 12, bottom: 4, trailing: 12))
                                .onTapGesture {
                                    closeAction()
                                }
                        }
                        Spacer()
                    }
                }
                .background(Color(UIColor.Theme.primaryContrast))
                .cornerRadius(radius: 8, corners: [.topLeft, .topRight])
            }
        }
        .frame(height: 120)
    }
}
