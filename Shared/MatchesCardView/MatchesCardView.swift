//
//  MatchesCardView.swift
//  ComponentCreation (iOS)
//
//  Created by Ashish Mankar on 12/01/22.
//

import SwiftUI

struct MatchesCardViewModel {
    
    func getBoldListFlag() -> Bool {
        return true
    }
    
    func getPremiumPlus() -> String {
        return "+"
    }
    
    func getPremiumTitle() -> String {
        return "PREMIUM"
    }
    
    func getPremiumIcon() -> UIImage {
        return UIImage(named: "membership_crown") ?? UIImage()
    }
    
    func getTrustBadgeOption() -> [String] {
        return ["Mobile number Verified"]
    }
    
    func getSecondLineBasicInfo() -> [String] {
        return ["32 yrs, 5'6\"","Landscape Engineer"]
    }
    
    func getThirdLineBasicInfo() -> [String] {
        return ["Sinhal - Catholic","Hyderabad"]
    }
    
    func getYouAndProspectIcon() -> UIImage {
        return UIImage(named: "youAndProspect") ?? UIImage()
    }
    
    func getYouAndProspectText() -> String {
        return "You & Him"
    }
    
    func getProspectMatch() -> [String] {
        return ["He enjoys non-vegetarian food too","You both are from the Marathi community"]
    }
    
    func getChatStatusColor() -> UIColor {
        return UIColor.Other.darkGreen
    }
    
    func getChatStatus() -> String {
        return "Online"
    }
    
    func getDisplayName() -> String {
        return "Manish A"
    }
    
    func getTrustBadgeImage() -> UIImage? {
        return UIImage(named: "trust_badge_green") ?? nil
    }
    
    func getConnectIcon() -> UIImage {
        return UIImage(named: "callToAction_Connect") ?? UIImage()
    }
    
    func ctaConnectColor() -> UIColor {
        return UIColor.Accent.primary
    }
    
    func getJustJoinTickIcon() -> UIImage {
        return UIImage(named: "just_join_tick") ?? UIImage()
    }
    
    func getVIPIcon() -> UIImage {
        return UIImage(named: "CTA_VIP_Gold_Interest") ?? UIImage()
    }
    
    func getPhotoCaseImage() -> UIImage {
        return UIImage(named: "demoNoPhotoCase") ?? UIImage()
    }
    
    func getPhotoLockImage() -> UIImage {
        return UIImage(named: "profileStatusLock") ?? UIImage()
    }
    
    func getPhotoHiddenTitle() -> String {
        return "Deepika has chosen to show her Photo only to Matches she is connected to"
        //return "Deepika has hidden her photo for Free members"
    }
    
    func getPhotoCaseTitle() -> String {
        //return "Photo Coming Soon"
        //return "You sent him a Photo Request"
        return "Photo not added"
    }
    
    func isUpgradeToViewPhoto() -> Bool {
        return false
    }
    
    func isRequestPhoto() -> Bool {
        return true
    }
    
    func getVIPCaptionColor() -> UIColor {
        return UIColor.Theme.primaryContrast
    }
    
    func ctaJustJoinConnectColor() -> UIColor {
        return UIColor.Theme.primary
    }
    
    func ctaNotNowColor() -> UIColor {
        return UIColor.Other.Gray.textColor
    }
    
    func ctaCurveColor() -> UIColor {
        return UIColor.Neutrals.divider
    }
    
    func getMembershipTagColor() -> UIColor {
        return UIColor.Theme.primary
    }
    
    func getProfilePicture() -> UIImage {
        return UIImage(named: "male_member") ?? UIImage()
    }
    
    func getPictureCount() -> Int {
        return 5
    }
    
    func getCardWidth() -> CGFloat {
        let margin:CGFloat = 10
        return UIScreen.main.bounds.size.width - (margin * 2)
    }
    
    func getPitchBackgroundWidth() -> CGFloat {
        return UIScreen.main.bounds.size.width
    }
    
    func getPitchBackgroundHeight() -> CGFloat {
        return getCardWidth()/2.2
    }
    
    func getPitchMargin() -> CGFloat {
        return 10
    }
    
    func getCardHeight() -> CGFloat {
        return getCardWidth() * 501/375
    }
    
    func getVIPTagWidth() -> CGFloat {
        return self.getCardWidth()/2.44
    }
    
    func getCTARadius() -> CGFloat {
        return self.getCardWidth()/6.1
    }
    
    func getCTAMargin() -> CGFloat {
        return self.getCardWidth()/10
    }
    
    func getCTAToolTipWidth() -> CGFloat {
        return self.getCardWidth()/1.53
    }
    
    func getPhotoCaseOffSet() -> CGFloat {
        return self.getCardWidth()/6.06
    }
    
    func getOnboardingIconMargin() -> CGFloat {
        return self.getCardWidth()/5.15
    }
    
    func isJustJoined() -> Bool {
        return true
    }
    
    func getConnectPremiumPitchPhotoHieghtWidth() -> CGFloat {
        return self.getCardWidth()/3
    }
    
    func getRadiusForPremiumPitchPhoto() -> CGFloat {
        return getConnectPremiumPitchPhotoHieghtWidth()/2
    }
    
    func getPhotoCaseWidth() -> CGFloat {
        return UIScreen.main.bounds.width / 3.75
    }
}

struct MatchesCardView: View {
    @State var toolTipFlag:Bool = false
    var chatStatusAction:()->()
    var youAndProspectAction:()->()
    var optionAction:()->()
    var albumAction:()->()
    var righCTAAction:()->()
    var leftCTAAction:()->()
    var body: some View {
        let viewModel:MatchesCardViewModel = MatchesCardViewModel()
        ZStack {
            let width = viewModel.getCardWidth()
            let height = width * (420/340)
            VStack{
                MatchesSwipeHeaderView()
                    .padding(.horizontal, 10)
                    .zIndex(1)
                ZStack(alignment: .center){
                    ZStack(alignment:.top){
                        MatchesCardProfilePicture()
                        MatchesCardGradientView(height: height)
                        VStack(alignment:.trailing){
                            MembershipTagView(color: viewModel.getMembershipTagColor())
                            BasicInfoView(trustBadgeAction: {
                                toolTipFlag.toggle()
                            })
                            CallToActionView(width: width,rightCTAAction: righCTAAction,leftCTAAction: leftCTAAction)
                        }
                        HStack{
                            Spacer()
                            OptionsAlbumView(photoCount: viewModel.getPictureCount())
                        }
                    }
                    .cornerRadius(8)
                    .frame(width:width, height: height)
                    if toolTipFlag {
                        MatchesCardToolTipView(width: width,height: height)
                            .onTapGesture {
                                toolTipFlag.toggle()
                            }
                            .zIndex(1)
                    }
                }
            }
        }
    }
}

struct MatchesCardView_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            MatchesCardView(chatStatusAction: {}, youAndProspectAction: {}, optionAction: {}, albumAction: {}, righCTAAction: {}, leftCTAAction: {})
            RedBackground()
        }
    }
}

struct CornerCurve: View {
    var color:UIColor = .red
    var body: some View {
        Path{ path in
            path.move(to: CGPoint(x: 0, y: 0))
            path.addLine(to: CGPoint(x: 0, y: 12))
            path.addCurve(to: CGPoint(x: 12, y: 0), control1: CGPoint(x: 0, y: 0), control2: CGPoint(x: 0, y: 0))
            path.closeSubpath()
        }
        .foregroundColor(Color(color))
        .frame(width: 12,height: 16)
    }
}

struct MembershipTagView: View {
    var viewModel:MatchesCardViewModel = MatchesCardViewModel()
    var color:UIColor =  UIColor.Theme.primary
    var body: some View {
        let height:CGFloat = viewModel.getBoldListFlag() ? 4 : 0
        let topMargin:CGFloat = viewModel.getBoldListFlag() ? 6 : 10
        let isBoldListing = viewModel.getBoldListFlag()
        ZStack(alignment:.topLeading){
            if isBoldListing {
                VStack(alignment: .leading,spacing: 0){
                    Color(color)
                        .frame(height: height)
                    HStack{
                        CornerCurve(color: color)
                            .offset(y: 0)
                        Spacer()
                        CornerCurve(color: color)
                            .rotationEffect(.degrees(90))
                            .offset(x: -2, y: -2)
                    }
                }
            }
            VStack(alignment: .leading, spacing: 0){
                HStack(alignment: .top,spacing: 0){
                    HStack(spacing: 0) {
                        Image(uiImage: viewModel.getPremiumIcon())
                            .resizable()
                            .renderingMode(.template)
                            .foregroundColor(.white)
                            .frame(width:10, height: 10)
                            .padding(EdgeInsets(top: 0, leading: 8, bottom: 0, trailing: 4))
                        Text(viewModel.getPremiumTitle())
                            .foregroundColor(.white)
                            .font(.system(size: 10, weight: .bold, design: .default))
                            .tracking(-1)
                            .offset(y: 1)
                        Text(viewModel.getPremiumPlus())
                            .font(.system(size: 10, weight: .bold, design: .default))
                            .foregroundColor(.white)
                            .padding(EdgeInsets(top: 0, leading: 2, bottom: 0, trailing: 6))
                            .offset(y: 1)
                    }
                    .padding(EdgeInsets(top: topMargin, leading: 0, bottom: 6, trailing: 0))
                    .background(Color(color))
                    .cornerRadius(radius: 8, corners: [.bottomRight])
                    .offset(y: -4)
                    CornerCurve(color: color)
                        //.offset(y: isBoldListing ? 0 : -4)
                    Spacer()
                }
                CornerCurve(color: color)
                    .offset(y: -4)
            }
            .offset(y: isBoldListing ? 4 : 0)
            JustJoinTag(viewModel: viewModel)
        }
    }
}

struct OptionsAlbumView: View {
    var photoCount:Int = 5
    var body: some View {
        VStack(alignment: .trailing, spacing: 20){
            Button(action: {}, label: {
                ZStack{
                    Circle()
                        .foregroundColor(.black)
                        .opacity(0.36)
                    Image("more")
                        .resizable()
                        .frame(width: 16, height: 4)
                }
            })
            .frame(width: 30,height: 30)
            .padding(EdgeInsets(top: 0, leading: 0, bottom: 0, trailing: 10))
            if photoCount > 0 {
                Button(action: {}, label: {
                    HStack(spacing: 0){
                        Image("profile_camera")
                            .resizable()
                            .scaledToFit()
                            .frame(width: 16, height: 14)
                            .padding(EdgeInsets(top: 0, leading: 4, bottom: 0, trailing: 2))
                        Text("\(photoCount)")
                            .font(.system(size: 12, weight: .bold, design: .default))
                            .foregroundColor(.white)
                            .padding(EdgeInsets(top: 0, leading: 4, bottom: 0, trailing: 4))
                    }
                    .padding(8)
                    .background(Color(.black).opacity(0.36))
                    .cornerRadius(16)
                })
            }
        }
        .offset(y: 22)
        .padding(EdgeInsets(top: 0, leading: 0, bottom: 0, trailing: 12))
    }
}

struct BasicInfoView: View {
    var viewModel:MatchesCardViewModel = MatchesCardViewModel()
    var trustBadgeAction: ()->()
    @State private var isSolid:Bool = true
    var body: some View {
        VStack(alignment: .leading,spacing: 6){
            Spacer()
            HStack{
                if let image = viewModel.getTrustBadgeImage() {
                    Button(action: trustBadgeAction, label: {
                        Image(uiImage: image)
                            .resizable()
                            .scaledToFit()
                            .frame(width: 16,height: 18)
                    })
                }
                Text(viewModel.getDisplayName())
                    .foregroundColor(.white)
                    .font(.system(size: 18, weight: .bold, design: .default))
                    .lineLimit(1)
                Button(action: {}, label: {
                    HStack(spacing: 4){
                        let color:Color = Color(viewModel.getChatStatusColor())
                        ZStack{
                            Circle()
                                .foregroundColor(color)
                                .opacity(isSolid ? 1 : 0)
                                .animation(Animation.linear(duration: 1).repeatForever().delay(2))
                            Circle()
                                .stroke(color, lineWidth: 1)
                        }
                        .frame(width: 10,height: 10)
                        Text(viewModel.getChatStatus())
                            .foregroundColor(.white)
                            .font(.system(size: 10, weight: .medium, design: .default))
                    }
                    .padding(EdgeInsets(top: 4, leading: 6, bottom: 4, trailing: 6))
                    .background(Color(.black).opacity(0.36))
                    .cornerRadius(radius: 6, corners: [.topLeft,.bottomLeft])
                    .cornerRadius(radius: 9, corners: [.topRight,.bottomRight])
                    
                })
                if self.viewModel.getProspectMatch().count > 0 {
                    Button(action: {}, label: {
                        HStack(spacing: 4){
                            Image(uiImage: viewModel.getYouAndProspectIcon())
                                .resizable()
                                .scaledToFit()
                                .frame(width: 12,height: 8)
                            Text(viewModel.getYouAndProspectText())
                                .foregroundColor(.white)
                                .font(.system(size: 10, weight: .medium, design: .default))
                        }
                        .padding(EdgeInsets(top: 4, leading: 6, bottom: 4, trailing: 6))
                        .background(Color(.black).opacity(0.36))
                        .cornerRadius(radius: 6, corners: [.topLeft,.bottomLeft])
                        .cornerRadius(radius: 9, corners: [.topRight,.bottomRight])
                    })
                }
                Spacer()
            }
            HStack(spacing: 4) {
                ForEach(Array(viewModel.getSecondLineBasicInfo().enumerated()), id: \.element) { index, element in
                    Text(element)
                        .font(.system(size: 14, weight: .regular, design: .default))
                        .foregroundColor(.white)
                    if index < 1 && viewModel.getSecondLineBasicInfo().count > 1 {
                        Circle()
                            .foregroundColor(.white)
                            .opacity(0.6)
                            .frame(width:4,height: 4)
                    }
                }
            }
            HStack(spacing: 4) {
                ForEach(Array(viewModel.getThirdLineBasicInfo().enumerated()), id: \.element) { index, element in
                    Text(element)
                        .font(.system(size: 14, weight: .regular, design: .default))
                        .foregroundColor(.white)
                    if index < 1 && viewModel.getThirdLineBasicInfo().count > 1 {
                        Circle()
                            .foregroundColor(.white)
                            .opacity(0.6)
                            .frame(width:4,height: 4)
                    }
                }
            }
        }
        .onAppear{
            isSolid = false
        }
        .padding(EdgeInsets(top: 0, leading: 20, bottom: 0, trailing: 20))
    }
}

struct CallToActionView: View {
    var width:CGFloat = 0
    var viewModel:MatchesCardViewModel = MatchesCardViewModel()
    var rightCTAAction:()->()
    var leftCTAAction:()->()
    var body: some View {
        ZStack{
            ActionEOIView(leftCTAAction: leftCTAAction, rightCTAAction: rightCTAAction)
        }
    }
}

struct MatchesCardToolTipView: View {
    var viewModel:MatchesCardViewModel = MatchesCardViewModel()
    var width:CGFloat = 0
    var height:CGFloat = 0
    var body: some View {
        ZStack(alignment: .bottomLeading){
            Color(.clear)
            HStack{
                VStack(alignment:.leading,spacing: 0){
                    ZStack{
                        VStack(alignment: .leading,spacing: 0){
                            ForEach(Array(viewModel.getTrustBadgeOption().enumerated()), id: \.element) { index, element in
                                HStack{
                                    Image("NotificationTick")
                                    Text(element)
                                        .foregroundColor(.white)
                                        .font(.system(size: 14, weight: .semibold, design: .default))
                                }
                                .padding(EdgeInsets(top: 2, leading: 16, bottom: 2, trailing: 16))
                            }
                        }
                    }
                    .padding(EdgeInsets(top: 12, leading: 0, bottom: 12, trailing: 0))
                    .background(Color(UIColor.Other.blackShade))
                    .cornerRadius(8)
                    Image("coachUpArrow")
                        .rotationEffect(.degrees(180))
                        .padding(EdgeInsets(top: 0, leading: 8, bottom: 0, trailing: 0))
                }
                .padding(EdgeInsets(top: 0, leading: 14, bottom: 0, trailing: 0))
                Spacer()
            }
            .offset(y: -185)
        }
        .frame(width: width, height: height)
    }
}

struct MatchesCardGradientView: View {
    var height:CGFloat = 0
    var body: some View {
        VStack{
            Spacer()
            LinearGradient(gradient: Gradient(colors: [Color.clear, Color.black]), startPoint: .top, endPoint: .bottom)
                .frame(height:height/2)
        }
    }
}

struct MatchesCardProfilePicture: View {
    var viewModel:MatchesCardViewModel = MatchesCardViewModel()
    var body: some View {
        Image(uiImage: viewModel.getProfilePicture())
            .resizable()
            .clipShape(Rectangle())
    }
}

struct JustJoinTag: View {
    var viewModel:MatchesCardViewModel
    var body: some View {
        if viewModel.isJustJoined() {
            Image("just_joined_profile")
                .offset(x: -6, y: 32)
        }
    }
}

struct RedBackground: View {
    var body: some View {
        Color(UIColor.Theme.primaryContrast)
    }
}
